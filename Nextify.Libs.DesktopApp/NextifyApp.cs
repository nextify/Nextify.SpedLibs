﻿using Newtonsoft.Json;
using Nextify.Libs.DesktopApp.App;
using System;
using System.Windows.Forms;

namespace Nextify.Libs.DesktopApp
{
    public partial class NextifyApp : Form
    {
        public void Inicializar()
        {
            if (!AuthApp.LoginValido())
            {
                NextifyLogin t = new NextifyLogin();
                t.StartPosition = FormStartPosition.CenterScreen;
                if (t.ShowDialog() == DialogResult.OK)
                {
                    NextifySettings.Inicializar();
                    Inicializar();
                }
                else
                {
                    if (!AuthApp.LoginValido()) Application.Exit();
                }
            }
            // SetWindowText();
        }

        public NextifyApp()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void sairToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void NextifyApp_Load(object sender, EventArgs e)
        {
            Inicializar();
        }
    }
}
