﻿using Newtonsoft.Json;
using Nextify.Libs.DesktopApp.Classes.Model;
using Nextify.Libs.Reinf.Classes.Api;
using Nextify.Libs.Reinf.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nextify.Libs.DesktopApp.App
{
    public class EmpresaApp
    {
        public static List<Empresa> ObterEmpresas()
        {
            using (NextifyContext db = new NextifyContext())
            {
                var list = db.Empresa.Where(n => n.ativo == true).ToList();

                if (list.Count > 0) return list;
                else { 
                    try
                    {
                        ObterEmpresasApi();
                    } catch(Exception ex)
                    {
                        MessageBox.Show("Ocorreu um erro ao tentar obter listagem de empresas.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                list = db.Empresa.Where(n => n.ativo == true).ToList();

                return list;
            }
        }

        public static void ObterEmpresasApi()
        {
            RestClient rest = new RestClient();

            rest.endPoint = "http://127.0.0.1:3000/v1/empresa/list";
            rest.AuthData = new AuthData { AuthenticationType = AuthenticationType.Bearer, AuthToken = AuthApp.ObterToken() };
            rest.httpMethod = HttpVerb.GET;

            try
            {
                var output = rest.MakeRequest();
                List<EmpresaObject> empresas = JsonConvert.DeserializeObject<List<EmpresaObject>>(output.data);

                foreach (var empresa in empresas)
                {
                    using (NextifyContext db = new NextifyContext())
                    {
                        var model = db.Empresa.Where(n => n._id == empresa._id).SingleOrDefault();

                        if (model == null) {
                            model = new Empresa();
                            db.Empresa.Add(model);
                        }

                        model._id = empresa._id;
                        model.nome = empresa.nome;
                        model.principal = empresa.principal;
                        model.ativo = true; // FIX

                        db.SaveChanges();
                    }
                }

            } catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao tentar obter listagem de empresas.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            


        }
    }
}
