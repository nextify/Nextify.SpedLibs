﻿using Newtonsoft.Json;
using Nextify.Libs.DesktopApp.Classes.Data;
using Nextify.Libs.Reinf.Classes.Api;
using Nextify.Libs.Reinf.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nextify.Libs.DesktopApp.App
{
    public class AuthApp
    {
        public static bool Login(string username, string password)
        {
            string login = "{ \"username\": \"" + username + "\", \"password\": \"" + password + "\" }";
            RestClient rest = new RestClient();

            rest.AuthData = new AuthData { AuthenticationType = AuthenticationType.Bearer, AuthToken = AuthApp.ObterToken() };
            rest.httpMethod = HttpVerb.POST;
            rest.endPoint = "http://127.0.0.1:3000/v1/auth/login";
            rest.postJSON = login;

            var output = rest.MakeRequest();

            switch (output.respStatus)
            {
                case 200:
                    AuthInfoObject info = JsonConvert.DeserializeObject<AuthInfoObject>(output.data);

                    AuthInfo auth = new AuthInfo
                    {
                        AccessToken = info.access_token,
                        TokenType = info.token_type,
                        RefreshToken = info.refresh_token,
                        ExpiresIn = info.expires_in
                    };

                    NextifySettings.AuthInfo = auth;
                    return true;
                case 401:
                    throw new NextifyException("Login não autorizado, dados inválidos.");
                case 500:
                    throw new NextifyException("Ocorreu um erro ao tentar efeturar o login, por favor tente mais tarde.");
                case -999:
                    throw new NextifyException("Ocorreu um erro ao tentar efetuar o login, por favor verifique os dados e tente novamente.");
                default:
                    throw new NextifyException("Ocorreu um erro ao tentar efeturar o login, por favor tente mais tarde.");
            }
        }

        public static bool Logout()
        {
            NextifySettings.AuthInfo = null;
            return (!LoginValido());
        }

        public static bool LoginValido()
        {
            return (NextifySettings.AuthInfo != null);
        }

        public static string ObterToken()
        {
            if (LoginValido()) return NextifySettings.AuthInfo.AccessToken;
            return null;
        }
    }
}
