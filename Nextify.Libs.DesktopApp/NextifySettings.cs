﻿using Nextify.Libs.DesktopApp.App;
using Nextify.Libs.DesktopApp.Classes.Data;
using Nextify.Libs.DesktopApp.Classes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Nextify.Libs.DesktopApp
{
    public static class NextifySettings
    {
        private static List<Empresa> listaEmpresa;

        public static List<Empresa> ListaEmpresas
        {
            get { return listaEmpresa; }
            set { listaEmpresa = value; }
        }

        private static Empresa empresaSelecionada;

        public static Empresa EmpresaSelecionada
        {
            get { return empresaSelecionada; }
            set { empresaSelecionada = value; }
        }


        private static X509Certificate2 certificate;

        public static X509Certificate2 Certificate
        {
            get
            {
                if (certificate == null)
                {
                    X509Store store = new X509Store("My", StoreLocation.CurrentUser);

                    store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

                    X509Certificate2Collection collection = store.Certificates;
                    X509Certificate2Collection fcollection = collection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
                    X509Certificate2Collection scollection = X509Certificate2UI.SelectFromCollection(fcollection, "Selecione um certificado", "Selecione um certificado para realizar assinatura dos Eventos:", X509SelectionFlag.SingleSelection);

                    if (scollection.Count == 0) return null;
                    certificate = scollection[0];
                }
                return certificate;
            }

            set
            {
                certificate = value;
            }
        }

        private static AuthInfo authInfo;

        public static AuthInfo AuthInfo {
            get { return authInfo; }
            set { authInfo = value; }
        }

        public static void Inicializar()
        {
            using (NextifyContext db = new NextifyContext())
            {
                ListaEmpresas = EmpresaApp.ObterEmpresas();
            }
            EmpresaSelecionada = ListaEmpresas.FirstOrDefault(n => n.principal == true);
        }

        public static void SelecionarEmpresa()
        {

        }
    }
}
