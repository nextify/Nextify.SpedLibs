﻿using Nextify.Libs.DesktopApp.App;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nextify.Libs.DesktopApp
{
    public partial class NextifyLogin : Form
    {
        public NextifyLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUsername.Text))
            {
                MessageBox.Show("O campo \"Nome de Usuário/Email\" é obrigatório.", "Erro de Validação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("O campo \"Senha\" é obrigatório.", "Erro de Validação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                if (AuthApp.Login(txtUsername.Text, txtPassword.Text))
                {
                    DialogResult = DialogResult.OK;
                }
            } catch (NextifyException ex)
            {
                MessageBox.Show(ex.Message, "Erro de Login", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }


    }
}
