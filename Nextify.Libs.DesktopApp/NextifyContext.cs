﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nextify.Libs.DesktopApp.Classes.Model;

namespace Nextify.Libs.DesktopApp
{
    public partial class NextifyContext : DbContext
    {
        public DbSet<Empresa> Empresa { get; set; }
        public DbSet<Estabelecimento> Estabelecimento { get; set; }
        public DbSet<Evento> Evento { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
