namespace Nextify.Libs.DesktopApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NextifyV00001 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empresa",
                c => new
                    {
                        EmpresaId = c.Int(nullable: false, identity: true),
                        _id = c.String(maxLength: 4000),
                        nome = c.String(maxLength: 4000),
                        principal = c.Boolean(nullable: false),
                        ativo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Estabelecimento",
                c => new
                    {
                        EstabelecimentoId = c.Int(nullable: false, identity: true),
                        _id = c.String(maxLength: 4000),
                        EmpresaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EstabelecimentoId)
                .ForeignKey("dbo.Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Evento",
                c => new
                    {
                        EventoId = c.Int(nullable: false, identity: true),
                        EmpresaId = c.Int(nullable: false),
                        EstabelecimentoId = c.Int(nullable: false),
                        _id = c.String(maxLength: 4000),
                        IdentificacaoEvento = c.String(maxLength: 4000),
                        ArquivoXml = c.String(),
                    })
                .PrimaryKey(t => t.EventoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Estabelecimento", "EmpresaId", "dbo.Empresa");
            DropIndex("dbo.Estabelecimento", new[] { "EmpresaId" });
            DropTable("dbo.Evento");
            DropTable("dbo.Estabelecimento");
            DropTable("dbo.Empresa");
        }
    }
}
