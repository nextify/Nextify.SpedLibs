﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nextify.Libs.DesktopApp.Enum
{
    public enum SituacaoLote
    {
        LotePronto,
        LoteEnviadoSucesso,
        LoteEnviadoErro,
        LoteProcessadoSucesso,
        LoteProcessadoErro
    }
}
