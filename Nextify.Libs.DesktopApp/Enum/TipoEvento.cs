﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nextify.Libs.DesktopApp.Enum
{
    public enum TipoEvento
    {
        R1000 = 0,
        R1070 = 1,
        R2010 = 2,
        R2020 = 3,
        R2030 = 4,
        R2040 = 5,
        R2050 = 6,
        R2060 = 7,
        R2070 = 8,
        R2098 = 9,
        R2099 = 10,
        R3010 = 11,
        R9000 = 12
    }
}
