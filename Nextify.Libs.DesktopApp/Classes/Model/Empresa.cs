﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nextify.Libs.DesktopApp.Classes.Model
{
    public class Empresa
    {
        public int EmpresaId { get; set; }

        public string _id { get; set; }

        public string nome { get; set; }

        public bool principal { get; set; }

        public bool ativo { get; set; }
    }
}
