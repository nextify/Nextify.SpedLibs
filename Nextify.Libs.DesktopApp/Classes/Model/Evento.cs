﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Nextify.Libs.DesktopApp.Enum;

namespace Nextify.Libs.DesktopApp.Classes.Model
{
    public class Evento
    {
        public int EventoId { get; set; }

        public int EmpresaId { get; set; }

        public int EstabelecimentoId { get; set; }

        public string _id { get; set; }

        public string IdentificacaoEvento { get; set; }

        public SituacaoEvento SituacaoEvento { get; set; }

        public TipoEvento TipoEvento { get; set; }

        [MaxLength]
        public string ArquivoXmlEnvio { get; set; }

        [MaxLength]
        public string ArquivoXmlRetorno { get; set; }

        public virtual Empresa Empresa { get; set; }

        public virtual Estabelecimento Estabelecimento { get; set; }

    }
}
