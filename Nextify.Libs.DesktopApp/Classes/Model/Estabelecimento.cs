﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nextify.Libs.DesktopApp.Classes.Model
{
    public class Estabelecimento
    {
        public int EstabelecimentoId { get; set; }

        public string _id { get; set; }

        public int EmpresaId { get; set; }

        public virtual Empresa Empresa { get; set; }

    }
}
