﻿using Nextify.Libs.DesktopApp.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nextify.Libs.DesktopApp.Classes.Model
{
    public class Lote
    {
        public int LoteId { get; set; }

        public int EmpresaId { get; set; }

        public SituacaoLote SituacaoLote { get; set; }

        [MaxLength]
        public string ArquivoXmlEnvio { get; set; }

        [MaxLength]
        public string ArquivoXmlRetorno { get; set; }

        public virtual Empresa Empresa { get; set; }
    }
}
