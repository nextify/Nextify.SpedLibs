﻿using System;
using System.Collections.Generic;
using System.Text;
using Nextify.Helpers;
using Nextify.WSUtils;
using Nextify.Libs.NFe.Classes.Xml.V4.NFe;
using Nextify.NFe.Settings;
using System.Xml;
using System.Security.Cryptography.X509Certificates;

namespace Nextify.NFe.Methods
{
    public static class NFeConsultaProtocolo4
    {
        public static TRetConsSitNFe NFeConsultaProtocolo(TConsSitNFe obj, string url, string method, X509Certificate2 cert)
        {
            var xml = SerializerHelper.Serialize<TConsSitNFe>(obj);

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);
            string ns = "http://www.portalfiscal.inf.br/nfe/wsdl/NFeConsultaProtocolo4";

            var response = WS.Request(xmldoc, url, method, ns, cert);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(response);

            XmlNamespaceManager xmlnsManager = new XmlNamespaceManager(xmlDoc.NameTable);

            xmlnsManager.AddNamespace("env", "http://www.w3.org/2003/05/soap-envelope");
            xmlnsManager.AddNamespace("si", ns);
            xmlnsManager.AddNamespace("ns", "http://www.portalfiscal.inf.br/nfe");

            XmlNode node = xmlDoc.SelectSingleNode("/env:Envelope/env:Body/si:nfeResultMsg/ns:retConsSitNFe", xmlnsManager);

            TRetConsSitNFe ret = SerializerHelper.Deserialize<TRetConsSitNFe>(node.OuterXml);

            return ret;
        }
    }
}
