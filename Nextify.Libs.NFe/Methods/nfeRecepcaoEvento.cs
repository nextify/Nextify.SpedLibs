﻿using Nextify.Helpers;
using Nextify.WSUtils;
using EventoCancelaNFe = Nextify.Libs.NFe.Classes.Xml.V4.EventoCancelaNFe;
using EventoCartaCorrecaoNFe = Nextify.Libs.NFe.Classes.Xml.V4.EventoCartaCorrecaoNFe;
using System.Xml;
using System.Security.Cryptography.X509Certificates;

namespace Nextify.NFe.Methods
{
    public class NFeRecepcaoEvento4 {
        public static EventoCancelaNFe.TRetEnvEvento NFeRecepcaoEventoNFCancela(EventoCancelaNFe.TEnvEvento obj, string url, string method, X509Certificate2 cert)
        {
            var xml = SerializerHelper.Serialize<EventoCancelaNFe.TEnvEvento>(obj);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);
            string ns = "http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4";

            var response = WS.Request(xmldoc, url, method, ns, cert);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(response);

            XmlNamespaceManager xmlnsManager = new XmlNamespaceManager(xmlDoc.NameTable);

            xmlnsManager.AddNamespace("env", "http://www.w3.org/2003/05/soap-envelope");
            xmlnsManager.AddNamespace("si", ns);
            xmlnsManager.AddNamespace("ns", "http://www.portalfiscal.inf.br/nfe");

            XmlNode node = xmlDoc.SelectSingleNode("/env:Envelope/env:Body/si:nfeResultMsg/ns:retEnvEvento", xmlnsManager);

            var ret = SerializerHelper.Deserialize<EventoCancelaNFe.TRetEnvEvento>(node.OuterXml);

            return ret;
        }

        public static EventoCartaCorrecaoNFe.TRetEnvEvento NFeRecepcaoEventoCCe(EventoCartaCorrecaoNFe.TEnvEvento obj, string url, string method, X509Certificate2 cert)
        {
            var xml = SerializerHelper.Serialize<EventoCartaCorrecaoNFe.TEnvEvento>(obj);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);
            string ns = "http://www.portalfiscal.inf.br/nfe/wsdl/NFeRecepcaoEvento4";

            var response = WS.Request(xmldoc, url, method, ns, cert);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(response);

            XmlNamespaceManager xmlnsManager = new XmlNamespaceManager(xmlDoc.NameTable);

            xmlnsManager.AddNamespace("env", "http://www.w3.org/2003/05/soap-envelope");
            xmlnsManager.AddNamespace("si", ns);
            xmlnsManager.AddNamespace("ns", "http://www.portalfiscal.inf.br/nfe");

            XmlNode node = xmlDoc.SelectSingleNode("/env:Envelope/env:Body/si:nfeResultMsg/ns:retEnvEvento", xmlnsManager);

            var ret = SerializerHelper.Deserialize<EventoCartaCorrecaoNFe.TRetEnvEvento>(node.OuterXml);

            return ret;
        }
    }
}
