﻿using Nextify.Libs.NFe.Classes;
using System;
using System.Collections.Generic;
using System.Text;
using Nextify.Libs.NFe.Classes.Xml.V4.TiposBasico;

namespace Nextify.Libs.NFe.Util
{
    public static class ChaveAcesso
    {
        public static TAmb ObterAmbiente(string ChaveAcesso)
        {
            return TAmb.HOMOLOGACAO;

        }

        public static TMod ObterModelo(string ChaveAcesso)
        {
            if (ChaveAcesso.Substring(20, 2) == "55") return TMod.Item55;
            else return TMod.Item65;
        }
    }
}
