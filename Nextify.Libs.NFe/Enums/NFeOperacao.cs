﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nextify.NFe.Enums
{
    public enum NFeOperacao
    {
        NFeAutorizacao,
        NFeStatusServico,
        NFeInutilizacao,
        NFeConsultaProtocolo,
        NFeConsultaCadastro,
        NFeRetAutorizacao,
        NFeRecepcaoEventoCCe,
        NFeRecepcaoEventoCancelamento,
    }
}
