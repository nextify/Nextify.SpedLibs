﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nextify.Libs.NFe.Classes.Xml.V4.TiposBasico
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public enum TAmb
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("1")]
        PRODUCAO = 1,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("2")]
        HOMOLOGACAO = 2,
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public enum TMod
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("55")]
        Item55 = 55,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("65")]
        Item65 = 65,
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public enum TCodUfIBGE
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("11")]
        UF11 = 11,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("12")]
        UF12 = 12,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("13")]
        UF13 = 13,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("14")]
        UF14 = 14,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("15")]
        UF15 = 15,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("16")]
        UF16 = 16,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("17")]
        UF17 = 17,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("21")]
        UF21 = 21,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("22")]
        UF22 = 22,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("23")]
        UF23 = 23,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("24")]
        UF24 = 24,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("25")]
        UF25 = 25,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("26")]
        UF26 = 26,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("27")]
        UF27 = 27,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("28")]
        UF28 = 28,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("29")]
        UF29 = 29,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("31")]
        UF31 = 31,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("32")]
        UF32 = 32,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("33")]
        UF33 = 33,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("35")]
        UF35 = 35,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("41")]
        UF41 = 41,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("42")]
        UF42 = 42,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("43")]
        UF43 = 43,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("50")]
        UF50 = 50,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("51")]
        UF51 = 51,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("52")]
        UF52 = 52,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("53")]
        UF53 = 53,
    }


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public enum TUf
    {

        /// <remarks/>
        AC,

        /// <remarks/>
        AL,

        /// <remarks/>
        AM,

        /// <remarks/>
        AP,

        /// <remarks/>
        BA,

        /// <remarks/>
        CE,

        /// <remarks/>
        DF,

        /// <remarks/>
        ES,

        /// <remarks/>
        GO,

        /// <remarks/>
        MA,

        /// <remarks/>
        MG,

        /// <remarks/>
        MS,

        /// <remarks/>
        MT,

        /// <remarks/>
        PA,

        /// <remarks/>
        PB,

        /// <remarks/>
        PE,

        /// <remarks/>
        PI,

        /// <remarks/>
        PR,

        /// <remarks/>
        RJ,

        /// <remarks/>
        RN,

        /// <remarks/>
        RO,

        /// <remarks/>
        RR,

        /// <remarks/>
        RS,

        /// <remarks/>
        SC,

        /// <remarks/>
        SE,

        /// <remarks/>
        SP,

        /// <remarks/>
        TO,

        /// <remarks/>
        EX,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public enum TUfEmi
    {

        /// <remarks/>
        AC,

        /// <remarks/>
        AL,

        /// <remarks/>
        AM,

        /// <remarks/>
        AP,

        /// <remarks/>
        BA,

        /// <remarks/>
        CE,

        /// <remarks/>
        DF,

        /// <remarks/>
        ES,

        /// <remarks/>
        GO,

        /// <remarks/>
        MA,

        /// <remarks/>
        MG,

        /// <remarks/>
        MS,

        /// <remarks/>
        MT,

        /// <remarks/>
        PA,

        /// <remarks/>
        PB,

        /// <remarks/>
        PE,

        /// <remarks/>
        PI,

        /// <remarks/>
        PR,

        /// <remarks/>
        RJ,

        /// <remarks/>
        RN,

        /// <remarks/>
        RO,

        /// <remarks/>
        RR,

        /// <remarks/>
        RS,

        /// <remarks/>
        SC,

        /// <remarks/>
        SE,

        /// <remarks/>
        SP,

        /// <remarks/>
        TO,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.portalfiscal.inf.br/nfe")]
    public enum TCOrgaoIBGE
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("11")]
        UF11,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("12")]
        UF12,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("13")]
        UF13,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("14")]
        UF14,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("15")]
        UF15,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("16")]
        UF16,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("17")]
        UF17,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("21")]
        UF21,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("22")]
        UF22,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("23")]
        UF23,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("24")]
        UF24,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("25")]
        UF25,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("26")]
        UF26,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("27")]
        UF27,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("28")]
        UF28,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("29")]
        UF29,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("31")]
        UF31,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("32")]
        UF32,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("33")]
        UF33,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("35")]
        UF35,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("41")]
        UF41,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("42")]
        UF42,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("43")]
        UF43,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("50")]
        UF50,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("51")]
        UF51,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("52")]
        UF52,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("53")]
        UF53,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("90")]
        UF90,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("91")]
        UF91,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("92")]
        UF92,
    }
}
