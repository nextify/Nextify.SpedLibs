﻿using System;
using System.Collections.Generic;
using System.Text;
using Nextify.Libs.NFe.Classes.Xml.V4.TiposBasico;

namespace Nextify.Libs.NFe.Classes
{
    public class NFeStatusServico
    {
        public TMod Modelo;

        public TAmb Ambiente;

        public string UF;

        public DateTime DataHoraConsulta;

        public DateTime DataHoraRetorno;

        public string Status;

        public string Mensagem;

        public string Observacao;

        public int TempoMedio;

        public double TempoUltimaConsulta()
        {
            DateTime startTime = DataHoraConsulta;
            DateTime endTime = DateTime.Now;
            TimeSpan span = endTime.Subtract(startTime);

            return span.TotalMinutes;
        }

        public double TempoUltimoRetorno()
        {
            DateTime startTime = DataHoraRetorno;
            DateTime endTime = DateTime.Now;
            TimeSpan span = endTime.Subtract(startTime);

            return span.TotalMinutes;
        }
    }


}
