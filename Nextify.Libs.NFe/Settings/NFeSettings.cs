﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Nextify.Libs.NFe.Classes;
using Nextify.Libs.NFe.Classes.Xml.V4.TiposBasico;
using Nextify.NFe.Enums;

namespace Nextify.NFe.Settings
{
    public class NFeConfigSefaz
    {
        public NFeConfigSefaz(TMod modelo, TAmb ambiente, string uf, NFeOperacao operacao, string url, string metodo, string urlqrcode, string urlconsulta)
        {
            Modelo = modelo;
            Ambiente = ambiente;
            Operacao = operacao;
            UF = uf;
            Url = url;
            Metodo = metodo;
            UrlQRCode = urlqrcode;
            UrlConsulta = urlconsulta;
        }

        public TMod Modelo;

        public TAmb Ambiente;

        public NFeOperacao Operacao;

        public string UF;

        public string Url;

        public string Metodo;

        public string UrlQRCode;

        public string UrlConsulta;
    }

    public class NFeConfig
    {
        public NFeConfig(TAmb ambiente, string dirbasexml, bool ativo)
        {
            Ambiente = ambiente;
            DirBaseXml = dirbasexml;
            Ativo = ativo;
        }

        public TAmb Ambiente;

        public string DirBaseXml;

        public bool Ativo;
    }

     public static class NFeSettings
    {
        public static List<NFeConfigSefaz> NFeConfigSefazList;

        private static List<NFeConfig> NFeConfigList;

        public static List<NFeStatusServico> NFeStatusList;

        static NFeSettings()
        {
            NFeConfigSefazList = new List<NFeConfigSefaz>();
            NFeConfigList = new List<NFeConfig>();
            NFeStatusList = new List<NFeStatusServico>();
        }

        public static void AddConfigSefaz(string Modelo, string Ambiente, string UF, string Operacao, string Url, string Metodo, string UrlQRCode, string UrlConsulta)
        {
            var nfe_config = new NFeConfigSefaz((TMod)Enum.Parse(typeof(TMod), Modelo),
                                           (TAmb)Enum.Parse(typeof(TAmb), Ambiente),
                                           UF,
                                           (NFeOperacao)Enum.Parse(typeof(NFeOperacao), Operacao),
                                           Url,
                                           Metodo,
                                           UrlQRCode,
                                           UrlConsulta);
            NFeConfigSefazList.Add(nfe_config);
        }

        public static void AddConfig(string ambiente, string dirbasexml, string ativo)
        {
            var config = new NFeConfig((TAmb)Enum.Parse(typeof(TAmb), ambiente), dirbasexml, bool.Parse(ativo));

            NFeConfigList.Add(config);
        }

        public static NFeConfigSefaz ObterConfigSefaz(TMod Modelo, TAmb Ambiente, string UF, NFeOperacao Operacao)
        {
            var config = NFeConfigSefazList.SingleOrDefault(n => n.Modelo == Modelo
                                                                && n.Ambiente == Ambiente
                                                                && n.UF == UF
                                                                && n.Operacao == Operacao);

            return config;
        }

        public static NFeConfig ObterConfig(TAmb Ambiente)
        {
            var config = NFeConfigList.SingleOrDefault(n => n.Ambiente == Ambiente);

            return config;
        }

        public static string ObterCaminhoNFe(TAmb Ambiente, string cnpj, string filename)
        {
            var config = ObterConfig(Ambiente);
            string[] paths = { config.DirBaseXml, cnpj, filename };
            return Path.Combine(paths);
        }

        public static Dictionary<string, TCodUfIBGE> UFCodIbge()
        {
            return new Dictionary<string, TCodUfIBGE>
            {
                { "RO", TCodUfIBGE.UF11 }, // RO
                { "AC", TCodUfIBGE.UF12 }, // AC
                { "AM", TCodUfIBGE.UF13 }, // AM
                { "RR", TCodUfIBGE.UF14 }, // RR
                { "PA", TCodUfIBGE.UF15 }, // PA
                { "AP", TCodUfIBGE.UF16 }, // AP
                { "TO", TCodUfIBGE.UF17 }, // TO
                { "MA", TCodUfIBGE.UF21 }, // MA
                { "PI", TCodUfIBGE.UF22 }, // PI
                { "CE", TCodUfIBGE.UF23 }, // CE
                { "RN", TCodUfIBGE.UF24 }, // RN
                { "PB", TCodUfIBGE.UF25 }, // PB
                { "PE", TCodUfIBGE.UF26 }, // PE
                { "AL", TCodUfIBGE.UF27 }, // AL
                { "SE", TCodUfIBGE.UF28 }, // SE
                { "BA", TCodUfIBGE.UF29 }, // BA
                { "MG", TCodUfIBGE.UF31 }, // MG
                { "ES", TCodUfIBGE.UF32 }, // ES
                { "RJ", TCodUfIBGE.UF33 }, // RJ
                { "SP", TCodUfIBGE.UF35 }, // SP
                { "PR", TCodUfIBGE.UF41 }, // PR
                { "SC", TCodUfIBGE.UF42 }, // SC
                { "RS", TCodUfIBGE.UF43 }, // RS
                { "MS", TCodUfIBGE.UF50 }, // MS
                { "MT", TCodUfIBGE.UF51 }, // MT
                { "GO", TCodUfIBGE.UF52 }, // GO
                { "DF", TCodUfIBGE.UF53 } }; // DF
        }

        public static Dictionary<string, TCOrgaoIBGE> UFCOrgaoIbge()
        {
            return new Dictionary<string, TCOrgaoIBGE>
            {
                { "RO", TCOrgaoIBGE.UF11 }, // RO
                { "AC", TCOrgaoIBGE.UF12 }, // AC
                { "AM", TCOrgaoIBGE.UF13 }, // AM
                { "RR", TCOrgaoIBGE.UF14 }, // RR
                { "PA", TCOrgaoIBGE.UF15 }, // PA
                { "AP", TCOrgaoIBGE.UF16 }, // AP
                { "TO", TCOrgaoIBGE.UF17 }, // TO
                { "MA", TCOrgaoIBGE.UF21 }, // MA
                { "PI", TCOrgaoIBGE.UF22 }, // PI
                { "CE", TCOrgaoIBGE.UF23 }, // CE
                { "RN", TCOrgaoIBGE.UF24 }, // RN
                { "PB", TCOrgaoIBGE.UF25 }, // PB
                { "PE", TCOrgaoIBGE.UF26 }, // PE
                { "AL", TCOrgaoIBGE.UF27 }, // AL
                { "SE", TCOrgaoIBGE.UF28 }, // SE
                { "BA", TCOrgaoIBGE.UF29 }, // BA
                { "MG", TCOrgaoIBGE.UF31 }, // MG
                { "ES", TCOrgaoIBGE.UF32 }, // ES
                { "RJ", TCOrgaoIBGE.UF33 }, // RJ
                { "SP", TCOrgaoIBGE.UF35 }, // SP
                { "PR", TCOrgaoIBGE.UF41 }, // PR
                { "SC", TCOrgaoIBGE.UF42 }, // SC
                { "RS", TCOrgaoIBGE.UF43 }, // RS
                { "MS", TCOrgaoIBGE.UF50 }, // MS
                { "MT", TCOrgaoIBGE.UF51 }, // MT
                { "GO", TCOrgaoIBGE.UF52 }, // GO
                { "DF", TCOrgaoIBGE.UF53 },
                { "SVAN", TCOrgaoIBGE.UF91 }}; // DF
        }

        public static Dictionary<TCodUfIBGE, string> UFAmbiente()
        {
            return new Dictionary<TCodUfIBGE, string>
            {
                { TCodUfIBGE.UF11, "SVRS" }, // RO
                { TCodUfIBGE.UF12, "SVRS" }, // AC
                { TCodUfIBGE.UF13, "AM" }, // AM
                { TCodUfIBGE.UF14, "SVRS" }, // RR
                { TCodUfIBGE.UF15, "SVAN" }, // PA
                { TCodUfIBGE.UF16, "SVRS" }, // AP
                { TCodUfIBGE.UF17, "SVRS" }, // TO
                { TCodUfIBGE.UF21, "SVAN" }, // MA
                { TCodUfIBGE.UF22, "SVRS" }, // PI
                { TCodUfIBGE.UF23, "CE" }, // CE
                { TCodUfIBGE.UF24, "SVRS" }, // RN
                { TCodUfIBGE.UF25, "SVRS" }, // PB
                { TCodUfIBGE.UF26, "PE" }, // PE
                { TCodUfIBGE.UF27, "SVRS" }, // AL
                { TCodUfIBGE.UF28, "SVRS" }, // SE
                { TCodUfIBGE.UF29, "BA" }, // BA
                { TCodUfIBGE.UF31, "MG" }, // MG
                { TCodUfIBGE.UF32, "SVRS" }, // ES
                { TCodUfIBGE.UF33, "SVRS" }, // RJ
                { TCodUfIBGE.UF35, "SP" }, // SP
                { TCodUfIBGE.UF41, "PR" }, // PR
                { TCodUfIBGE.UF42, "SVRS" }, // SC
                { TCodUfIBGE.UF43, "RS" }, // RS
                { TCodUfIBGE.UF50, "MS" }, // MS
                { TCodUfIBGE.UF51, "MT" }, // MT
                { TCodUfIBGE.UF52, "GO" }, // GO
                { TCodUfIBGE.UF53, "SVRS" } }; // DF
        }

        public static Dictionary<TCodUfIBGE, string> UFAmbienteContingencia()
        {
            return new Dictionary<TCodUfIBGE, string>
            {
                { TCodUfIBGE.UF11, "SVC-AN" }, // RO
                { TCodUfIBGE.UF12, "SVC-AN" }, // AC
                { TCodUfIBGE.UF13, "SVC-RS" }, // AM
                { TCodUfIBGE.UF14, "SVC-AN" }, // RR
                { TCodUfIBGE.UF15, "SVC-RS" }, // PA
                { TCodUfIBGE.UF16, "SVC-AN" }, // AP
                { TCodUfIBGE.UF17, "SVC-AN" }, // TO
                { TCodUfIBGE.UF21, "SVC-RS" }, // MA
                { TCodUfIBGE.UF22, "SVC-RS" }, // PI
                { TCodUfIBGE.UF23, "SVC-RS" }, // CE
                { TCodUfIBGE.UF24, "SVC-AN" }, // RN
                { TCodUfIBGE.UF25, "SVC-AN" }, // PB
                { TCodUfIBGE.UF26, "SVC-RS" }, // PE
                { TCodUfIBGE.UF27, "SVC-AN" }, // AL
                { TCodUfIBGE.UF28, "SVC-AN" }, // SE
                { TCodUfIBGE.UF29, "SVC-RS" }, // BA
                { TCodUfIBGE.UF31, "SVC-AN" }, // MG
                { TCodUfIBGE.UF32, "SVC-AN" }, // ES
                { TCodUfIBGE.UF33, "SVC-AN" }, // RJ
                { TCodUfIBGE.UF35, "SVC-AN" }, // SP
                { TCodUfIBGE.UF41, "SVC-RS" }, // PR
                { TCodUfIBGE.UF42, "SVC-AN" }, // SC
                { TCodUfIBGE.UF43, "SVC-AN" }, // RS
                { TCodUfIBGE.UF50, "SVC-RS" }, // MS
                { TCodUfIBGE.UF51, "SVC-RS" }, // MT
                { TCodUfIBGE.UF52, "SVC-RS" }, // GO
                { TCodUfIBGE.UF53, "SVC-AN" } }; // DF
        }

        //public string GetUFAmbiente(TCodUfIBGE ufibge, bool contingencia = false)
        //{
        //    Dictionary<TCodUfIBGE, string> dic = null;

        //    if (contingencia) dic = UFAmbienteContingencia();
        //    else dic = UFAmbiente();
        //    return dic[ufibge];
        //}

        //public string GetUrlName(TMod modelo, TAmb ambiente, string uf, NFeOperacao operacao, bool contingencia = false)
        //{
        //    return urldictionary[modelo][ambiente][uf][operacao].url;
        //}

        //public string GetUrlName(TMod modelo, TAmb ambiente, TCodUfIBGE ufibge, NFeOperacao operacao, bool contingencia = false)
        //{
        //    string uf = GetUFAmbiente(ufibge, contingencia);
        //    return urldictionary[modelo][ambiente][uf][operacao].url;
        //}

        //public string GetMethodName(TMod modelo, TAmb ambiente, string uf, NFeOperacao operacao, bool contingencia = false)
        //{
        //    return urldictionary[modelo][ambiente][uf][operacao].method;
        //}

        //public string GetMethodName(TMod modelo, TAmb ambiente, TCodUfIBGE ufibge, NFeOperacao operacao, bool contingencia = false)
        //{
        //    string uf = GetUFAmbiente(ufibge, contingencia);
        //    return urldictionary[modelo][ambiente][uf][operacao].method;
        //}
    }

}

