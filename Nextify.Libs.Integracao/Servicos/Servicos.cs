﻿using Nextify.Libs.Integracao.Interfaces;
using Nextify.Libs.Integracao.Enums;
using Nextify.Libs.Integracao.Classes;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace Nextify.Libs.Integracao.Services
{
    public class Servicos
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public IServices ObterIntegracao(TipoIntegracao tipo)
        {
            if (tipo == TipoIntegracao.NFeDjango) return new IntegracaoNFeDjango();
            else if (tipo == TipoIntegracao.NFeServices) return new IntegracaoNFe();
            else return null;
        }

        public void Execute()
        {
            var emitentes = Emitente.Config.Where(n => n.Ativo == true).ToList();
            
            foreach (var emitente in emitentes)
            {
                var sistema = IntegracaoSistemas.Sistemas.SingleOrDefault(n => n.Instancia == emitente.Instancia && n.Ativo == true);
                IServices svc = ObterIntegracao((TipoIntegracao)sistema.Tipointegracao);
                if (svc != null) svc.ExecutarIntegracao(emitente);
            }
            
        }
    }
}
