﻿using Nextify.Helpers;
using Nextify.Libs.Integracao.Classes;
using Nextify.Libs.NFe.Classes;
using Nextify.Libs.NFe.Classes.Xml.V4.NFe;
using EventoCartaCorrecaoNFe = Nextify.Libs.NFe.Classes.Xml.V4.EventoCartaCorrecaoNFe;
using EventoCancelaNFe = Nextify.Libs.NFe.Classes.Xml.V4.EventoCancelaNFe;
using Nextify.Libs.NFe.Classes.Xml.V4.TiposBasico;
using Nextify.NFe.Enums;
using Nextify.NFe.Methods;
using Nextify.NFe.Settings;
using Nextify.Util;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using System.Linq;

namespace Nextify.Libs.Integracao.Services
{
    public class ServicosNFe
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public void EnviarLoteNFe(EventoNFe evt, EmitenteConfig emitente, string xml)
        {
            _logger.Trace($"Iniciando Envio de Lote NFe: {evt.ChaveAcesso}, Ambiente: {evt.Ambiente}.");
            TAmb ambiente = (TAmb)evt.Ambiente;
            TMod modelo = NFe.Util.ChaveAcesso.ObterModelo(evt.ChaveAcesso);
            X509Certificate2 cert = emitente.certificate;

            var config_sefaz = NFeSettings.ObterConfigSefaz(modelo, ambiente, emitente.Uf, NFeOperacao.NFeAutorizacao);

            // Correçoes Layout
            xml = GerarResponsavelTecnico(xml);

            // Carregar e Assinar Documento
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);
            var signedXML = SignatureHelper.AssinarXML(xml, xmldoc["NFe"]["infNFe"].Attributes["Id"].InnerXml, cert);

            XmlDocument signedXmldoc = new XmlDocument();
            signedXmldoc.LoadXml(signedXML);
            TNFe nfe = SerializerHelper.Deserialize<TNFe>(signedXmldoc["NFe"].OuterXml);

            // Gera URL (Mod 65) e Serializa novamente com a URL
            if (modelo == TMod.Item65)
            {
                GerarNFCeUrl2(nfe, config_sefaz.UrlQRCode, config_sefaz.UrlConsulta, emitente.CscId, emitente.Csc);
                signedXML = SerializerHelper.Serialize<TNFe>(nfe);
            }

            // Salvar arquivo XML antes do Envio
            string filename = NFeSettings.ObterCaminhoNFe(ambiente, emitente.Cnpj, evt.ChaveAcesso);
            XmlUtil.SaveXml(signedXML, $"{filename}.xml");

            // Geracao do Lote para envio da Nota
            var lote = GerarLoteNFe(nfe);

            // Enviar NFe
            TRetEnviNFe ret = NFeAutorizacao4.NfeAutorizacaoLote(lote, config_sefaz.Url, config_sefaz.Metodo, cert);

            // Lote Processado
            if (ret.cStat == "104")
            {
                var prot = (TProtNFe)ret.Item;
                evt.Status = long.Parse(prot.infProt.cStat);
                evt.Mensagem = prot.infProt.xMotivo;

                // Autorizada Gera processo e salva XML
                if (prot.infProt.cStat == "100")
                {
                    evt.Protocolo = prot.infProt.nProt;
                    evt.DataProcessamento = DateTime.Parse(prot.infProt.dhRecbto).ToString("yyyy-MM-dd HH:mm:ss");

                    TNfeProc proc = new TNfeProc();
                    proc.NFe = nfe;
                    proc.protNFe = prot;

                    string nfeproc = SerializerHelper.Serialize<TNfeProc>(proc);
                    XmlUtil.SaveXml(nfeproc, $"{filename}-nfeProc.xml");
                }
            }
            else if (ret.cStat == "103" || ret.cStat == "105") // Lote em Processamento
            {
                var recibo = (TRetEnviNFeInfRec)ret.Item;
                evt.Status = long.Parse(ret.cStat);
                evt.Mensagem = ret.xMotivo;
                evt.Recibo = recibo.nRec;
            }
            else
            {
                evt.Status = long.Parse(ret.cStat);
                evt.Mensagem = ret.xMotivo;
            }
        }

        public void ConsultarSituacaoNFe(EventoNFe evt, EmitenteConfig emitente)
        {
            _logger.Trace($"Iniciando Consulta de NFe: {evt.ChaveAcesso}, Ambiente: {evt.Ambiente}.");
            X509Certificate2 cert = emitente.certificate;

            TAmb ambiente = (TAmb)evt.Ambiente;
            TMod modelo = NFe.Util.ChaveAcesso.ObterModelo(evt.ChaveAcesso);

            var config = NFeSettings.ObterConfigSefaz(modelo, ambiente, emitente.Uf, NFeOperacao.NFeConsultaProtocolo);

            TConsSitNFe sit = new TConsSitNFe();
            sit.chNFe = evt.ChaveAcesso;
            sit.tpAmb = ambiente;
            sit.versao = TVerConsSitNFe.Item400;
            sit.xServ = TConsSitNFeXServ.CONSULTAR;

            TRetConsSitNFe ret = NFeConsultaProtocolo4.NFeConsultaProtocolo(sit, config.Url, config.Metodo, cert);

            if (ret.cStat == "100") // NFe Autorizada
            {
                var prot = ret.protNFe;
                evt.Status = long.Parse(prot.infProt.cStat);
                evt.Mensagem = prot.infProt.xMotivo;
                evt.Protocolo = prot.infProt.nProt;
                evt.DataProcessamento = DateTime.Parse(prot.infProt.dhRecbto).ToString("yyyy-MM-dd HH:mm:ss");
            }
            else
            {
                evt.Status = long.Parse(ret.cStat);
                evt.Mensagem = ret.xMotivo;
            }
        }

        public void ConsultarStatusServico(EmitenteConfig emitente, NFeStatusServico status)
        {
            _logger.Trace($"Iniciando Consulta de STAUS SERVICO. UF: {status.UF}, Ambiente: {status.Ambiente}.");

            var amb = NFeSettings.NFeConfigSefazList.SingleOrDefault(n => n.Operacao == NFeOperacao.NFeStatusServico && n.UF == status.UF && n.Ambiente == status.Ambiente && n.Modelo == status.Modelo);
            var ufibge = NFeSettings.UFCodIbge();

            TConsStatServ serv = new TConsStatServ();
            serv.cUF = ufibge[status.UF];
            serv.tpAmb = status.Ambiente;
            serv.versao = "4.00";
            serv.xServ = TConsStatServXServ.STATUS;

            TRetConsStatServ ret = NFeStatusServico4.NFeStatusServicoNF(serv, amb.Url, amb.Metodo, emitente.certificate);

            status.DataHoraConsulta = DateTime.Parse(ret.dhRecbto);
            status.Status = ret.cStat;
            status.Mensagem = ret.xMotivo;
            status.Observacao = ret.xObs;
            status.TempoMedio = Int32.Parse(ret.tMed);
        }

        public void ConsultarProcessamentoLoteNFe(EventoNFe evt, EmitenteConfig emitente)
        {
            throw new NotImplementedException();
        }

        public void EventoCancelamentoNFe(EventoNFe evt, EmitenteConfig emitente)
        {
            _logger.Trace($"Iniciando Cancelamento de NFe: {evt.ChaveAcesso}, Ambiente: {evt.Ambiente}.");

            EventoCancelaNFe.TEvento cancela = new EventoCancelaNFe.TEvento();
            EventoCancelaNFe.TEnvEvento evento = new EventoCancelaNFe.TEnvEvento();

            var ufibge = NFeSettings.UFCOrgaoIbge();

            cancela.versao = "1.00";
            cancela.infEvento = new EventoCancelaNFe.TEventoInfEvento();
            cancela.infEvento.Id = $"ID110111{evt.ChaveAcesso}{evt.SequenciaEvento?.ToString("D2")}";
            cancela.infEvento.cOrgao = ufibge[emitente.Uf];
            cancela.infEvento.tpAmb = (TAmb)evt.Ambiente;
            cancela.infEvento.ItemElementName = EventoCancelaNFe.ItemChoiceType.CNPJ;
            cancela.infEvento.Item = evt.Cnpj;
            cancela.infEvento.chNFe = evt.ChaveAcesso;
            cancela.infEvento.dhEvento = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            cancela.infEvento.tpEvento = EventoCancelaNFe.TEventoInfEventoTpEvento.Item110111;
            cancela.infEvento.nSeqEvento = "1";
            cancela.infEvento.verEvento = EventoCancelaNFe.TEventoInfEventoVerEvento.Item100;

            cancela.infEvento.detEvento = new EventoCancelaNFe.TEventoInfEventoDetEvento();
            cancela.infEvento.detEvento.descEvento = EventoCancelaNFe.TEventoInfEventoDetEventoDescEvento.Cancelamento;
            cancela.infEvento.detEvento.nProt = evt.Protocolo;
            cancela.infEvento.detEvento.xJust = evt.Justificativa;

            evento.idLote = DateTime.Now.ToString("yyyyMMddHHmmss");
            evento.versao = "1.00";
            evento.evento = new EventoCancelaNFe.TEvento[1];
            

            // Carregar e Assinar Documento
            var xml = SerializerHelper.Serialize<EventoCancelaNFe.TEvento>(cancela);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);
            var signedXML = SignatureHelper.AssinarXML(xml, xmldoc["evento"]["infEvento"].Attributes["Id"].InnerXml, emitente.certificate);

            XmlDocument signedXmldoc = new XmlDocument();
            signedXmldoc.LoadXml(signedXML);
            EventoCancelaNFe.TEvento TEvt = SerializerHelper.Deserialize<EventoCancelaNFe.TEvento>(signedXmldoc["evento"].OuterXml);

            //// Salvar arquivo XML antes do Envio
            //string filename = NFeSettings.ObterCaminhoNFe((TAmb)evt.Ambiente, emitente.Cnpj, evento.idLote);
            //XmlUtil.SaveXml(signedXML, $"{filename}.xml");

            evento.evento[0] = TEvt;

            // Envio
            var config_sefaz = NFeSettings.ObterConfigSefaz((TMod)evt.Modelo, (TAmb)evt.Ambiente, emitente.Uf, NFeOperacao.NFeRecepcaoEventoCancelamento);
            var ret = NFeRecepcaoEvento4.NFeRecepcaoEventoNFCancela(evento, config_sefaz.Url, config_sefaz.Metodo, emitente.certificate);

            if (ret.cStat == "128")
            {
                evt.Status = long.Parse(ret.retEvento[0].infEvento.cStat);
                evt.Mensagem = ret.retEvento[0].infEvento.xMotivo;
                if (ret.retEvento[0].infEvento.cStat == "135" || ret.retEvento[0].infEvento.cStat == "136")
                {
                    evt.Protocolo = ret.retEvento[0].infEvento.nProt;
                    evt.DataProcessamento = DateTime.Parse(ret.retEvento[0].infEvento.dhRegEvento).ToString("yyyy-MM-dd HH:mm:ss");

                    // Salvar Processo no arquivo XML
                    EventoCancelaNFe.TProcEvento proc = new EventoCancelaNFe.TProcEvento();
                    proc.versao = "4.00";
                    proc.evento = TEvt;
                    proc.retEvento = ret.retEvento[0];

                    var xmlproc = SerializerHelper.Serialize<EventoCancelaNFe.TProcEvento>(proc);
                    string filename = NFeSettings.ObterCaminhoNFe((TAmb)evt.Ambiente, emitente.Cnpj, evt.ChaveAcesso);
                    XmlUtil.SaveXml(xmlproc, $"{filename}_110111-procEventoNFe.xml");
                }
            }
        }

        public void EventoCCeNFe(EventoNFe evt, EmitenteConfig emitente)
        {
            _logger.Trace($"Iniciando Evento CCe NFe: {evt.ChaveAcesso}, Ambiente: {evt.Ambiente}.");

            if (evt.Modelo == 65)
            {
                evt.Status = 784;
                evt.Mensagem = "NFC - e não permite o evento de Carta de Correção";
                return;
            }

            EventoCartaCorrecaoNFe.TEvento cce = new EventoCartaCorrecaoNFe.TEvento();
            EventoCartaCorrecaoNFe.TEnvEvento evento = new EventoCartaCorrecaoNFe.TEnvEvento();

            var ufibge = NFeSettings.UFCOrgaoIbge();

            cce.infEvento = new EventoCartaCorrecaoNFe.TEventoInfEvento();
            cce.versao = "1.00";
            cce.infEvento.Id = $"ID110110{evt.ChaveAcesso}{evt.SequenciaEvento?.ToString("D2")}";
            cce.infEvento.cOrgao = ufibge[emitente.Uf];
            cce.infEvento.tpAmb = (TAmb)evt.Ambiente;
            cce.infEvento.ItemElementName = EventoCartaCorrecaoNFe.ItemChoiceType.CNPJ;
            cce.infEvento.Item = evt.Cnpj;
            cce.infEvento.chNFe = evt.ChaveAcesso;
            cce.infEvento.dhEvento = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
            cce.infEvento.tpEvento = EventoCartaCorrecaoNFe.TEventoInfEventoTpEvento.Item110110;
            cce.infEvento.nSeqEvento = "1";
            cce.infEvento.verEvento = EventoCartaCorrecaoNFe.TEventoInfEventoVerEvento.Item100;

            cce.infEvento.detEvento = new EventoCartaCorrecaoNFe.TEventoInfEventoDetEvento();
            cce.infEvento.detEvento.descEvento = EventoCartaCorrecaoNFe.TEventoInfEventoDetEventoDescEvento.CartadeCorrecao;
            cce.infEvento.detEvento.xCondUso = EventoCartaCorrecaoNFe.TEventoInfEventoDetEventoXCondUso.ACartadeCorrecaoedisciplinadapeloparagrafo1oAdoart7odoConvenioSNde15dedezembrode1970epodeserutilizadapararegularizacaodeerroocorridonaemissaodedocumentofiscaldesdequeoerronaoestejarelacionadocomIasvariaveisquedeterminamovalordoimpostotaiscomobasedecalculoaliquotadiferencadeprecoquantidadevalordaoperacaooudaprestacaoIIacorrecaodedadoscadastraisqueimpliquemudancadoremetenteoudodestinatarioIIIadatadeemissaooudesaida;
            cce.infEvento.detEvento.xCorrecao = evt.Justificativa;

            evento.idLote = DateTime.Now.ToString("yyyyMMddHHmmss");
            evento.versao = "1.00";
            evento.evento = new EventoCartaCorrecaoNFe.TEvento[1];


            // Carregar e Assinar Documento
            var xml = SerializerHelper.Serialize<EventoCartaCorrecaoNFe.TEvento>(cce);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xml);
            var signedXML = SignatureHelper.AssinarXML(xml, xmldoc["evento"]["infEvento"].Attributes["Id"].InnerXml, emitente.certificate);

            XmlDocument signedXmldoc = new XmlDocument();
            signedXmldoc.LoadXml(signedXML);
            EventoCartaCorrecaoNFe.TEvento TEvt = SerializerHelper.Deserialize<EventoCartaCorrecaoNFe.TEvento>(signedXmldoc["evento"].OuterXml);

            evento.evento[0] = TEvt;

            // Envio
            var config_sefaz = NFeSettings.ObterConfigSefaz((TMod)evt.Modelo, (TAmb)evt.Ambiente, emitente.Uf, NFeOperacao.NFeRecepcaoEventoCancelamento);
            var ret = NFeRecepcaoEvento4.NFeRecepcaoEventoCCe(evento, config_sefaz.Url, config_sefaz.Metodo, emitente.certificate);

            if (ret.cStat == "128")
            {
                evt.Status = long.Parse(ret.retEvento[0].infEvento.cStat);
                evt.Mensagem = ret.retEvento[0].infEvento.xMotivo;
                if (ret.retEvento[0].infEvento.cStat == "135" || ret.retEvento[0].infEvento.cStat == "136")
                {
                    evt.Protocolo = ret.retEvento[0].infEvento.nProt;

                    // Salvar Processo no arquivo XML
                    EventoCartaCorrecaoNFe.TProcEvento proc = new EventoCartaCorrecaoNFe.TProcEvento();
                    proc.versao = "4.00";
                    proc.evento = TEvt;
                    proc.retEvento = ret.retEvento[0];

                    var xmlproc = SerializerHelper.Serialize<EventoCartaCorrecaoNFe.TProcEvento>(proc);
                    string filename = NFeSettings.ObterCaminhoNFe((TAmb)evt.Ambiente, emitente.Cnpj, evt.ChaveAcesso);
                    XmlUtil.SaveXml(xmlproc, $"{filename}_110110-procEventoNFe.xml");
                }
            }
        }

        public static TEnviNFe GerarLoteNFe(TNFe nfe)
        {
            List<TNFe> lista = new List<TNFe>();
            TEnviNFe lote = new TEnviNFe();

            lista.Add(nfe);

            lote.indSinc = TEnviNFeIndSinc.Item1;
            lote.versao = "4.00";
            lote.NFe = lista.ToArray();
            lote.idLote = DateTime.Now.ToString("yyyyMMddHHmmss");

            return lote;
        }

        public static void GerarNFCeUrl(TNFe nfe, string urlQRCode, string urlConsulta, string cIdToken, string cscToken)
        {
            string cDest = (nfe.infNFe.dest != null) ? nfe.infNFe.dest.Item : null;
            if (nfe.infNFe.dest != null) cDest = nfe.infNFe.dest.Item;

            string chNFe = nfe.infNFe.Id.ToString().Replace("NFe", "");
            string nVersao = "100";
            string tpAmb = ((int)nfe.infNFe.ide.tpAmb).ToString();
            string dhEmi = HexadecimalEncoding.ToHexString(nfe.infNFe.ide.dhEmi);
            string vNF = nfe.infNFe.total.ICMSTot.vNF;
            string vICMS = nfe.infNFe.total.ICMSTot.vICMS;
            string digVal = HexadecimalEncoding.ToHexString(Convert.ToBase64String(nfe.Signature.SignedInfo.Reference.DigestValue));

            string param = $"chNFe={chNFe}" +
                $"&nVersao={nVersao}" +
                $"&tpAmb={tpAmb}";

            if (!string.IsNullOrEmpty(cDest)) param += $"&cDest={cDest}";

            param += $"&dhEmi={dhEmi}" +
                $"&vNF={vNF}" +
                $"&vICMS={vICMS}" +
                $"&digVal={digVal}" +
                $"&cIdToken={cIdToken}";


            string qrCode = $"{urlQRCode}?" + param + "&cHashQRCode=" + Hash.Sha1(param + cscToken);

            nfe.infNFeSupl = new TNFeInfNFeSupl();
            nfe.infNFeSupl.qrCode = qrCode;
            nfe.infNFeSupl.urlChave = urlConsulta;
        }

        public static string GerarResponsavelTecnico(string xml)
        {
            TNFe nfe = SerializerHelper.Deserialize<TNFe>(xml);

            nfe.infNFe.infRespTec = new TInfRespTec();
            nfe.infNFe.infRespTec.CNPJ = "13444582000147";
            nfe.infNFe.infRespTec.email = "contato@nextify.com.br";
            nfe.infNFe.infRespTec.fone = "41998519845";
            nfe.infNFe.infRespTec.xContato = "Carlos Meira";

            return SerializerHelper.Serialize<TNFe>(nfe);
        }

        public static void GerarNFCeUrl2(TNFe nfe, string urlQRCode, string urlConsulta, string cIdToken, string cscToken)
        {
            string cDest = (nfe.infNFe.dest != null) ? nfe.infNFe.dest.Item : null;
            if (nfe.infNFe.dest != null) cDest = nfe.infNFe.dest.Item;

            string chNFe = nfe.infNFe.Id.ToString().Replace("NFe", "");
            string nVersao = "2";
            string tpAmb = ((int)nfe.infNFe.ide.tpAmb).ToString();
            string digVal = HexadecimalEncoding.ToHexString(Convert.ToBase64String(nfe.Signature.SignedInfo.Reference.DigestValue));
            string param = $"{chNFe}|{nVersao}|{tpAmb}|{cIdToken.TrimStart(new char[] { '0' })}";

            string qrCode = $"{urlQRCode}?p=" + param + "|" + Hash.Sha1(param + cscToken);

            nfe.infNFeSupl = new TNFeInfNFeSupl();
            nfe.infNFeSupl.qrCode = qrCode;
            nfe.infNFeSupl.urlChave = urlConsulta;
        }
    }
}
