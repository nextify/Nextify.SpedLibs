﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nextify.Libs.Integracao.Classes
{
    public class NFeWebServiceStatus
    {
        [JsonProperty("webservice")]
        public string Webservice { get; set; }

        [JsonProperty("model")]
        public long Model { get; set; }

        [JsonProperty("environment")]
        public long Environment { get; set; }

        [JsonProperty("status_description")]
        public string StatusDescription { get; set; }

        [JsonProperty("query_date")]
        public string QueryDate { get; set; }

        [JsonProperty("status")]
        public long StatusId { get; set; }
    }

    public static partial class Serialize
    {
        public static string ToJson(this NFeWebServiceStatus self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }
}
