﻿using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Nextify.Libs.Integracao.Classes
{
    public partial class NFeList
    {
        [JsonProperty("EventoNFe")]
        public List<EventoNFe> EventoNFe { get; set; }
    }

    public partial class EventoNFe
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("nfe")]
        public long Nfe { get; set; }

        [JsonProperty("cnpj")]
        public string Cnpj { get; set; }

        [JsonProperty("modelo")]
        public long Modelo { get; set; }

        [JsonProperty("ambiente")]
        public long Ambiente { get; set; }

        [JsonProperty("operacao")]
        public long Operacao { get; set; }

        [JsonProperty("chave_acesso")]
        public string ChaveAcesso { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("mensagem")]
        public string Mensagem { get; set; }

        [JsonProperty("justificativa")]
        public string Justificativa { get; set; }

        [JsonProperty("protocolo")]
        public string Protocolo { get; set; }

        [JsonProperty("recibo")]
        public string Recibo { get; set; }

        [JsonProperty("evento")]
        public long Evento { get; set; }

        [JsonProperty("sequencia_evento")]
        public long? SequenciaEvento { get; set; }

        [JsonProperty("data_processamento")]
        public string DataProcessamento { get; set; }

    }

    public partial class NFeList
    {
        public static NFeList FromJson(string json) => JsonConvert.DeserializeObject<NFeList>(json, Converter.Settings);
    }

    public static partial class Serialize
    {
        public static string ToJson(this NFeList self) => JsonConvert.SerializeObject(self, Converter.Settings);

        public static string ToJson(this EventoNFe self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
