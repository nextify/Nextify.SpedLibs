﻿using Nextify.Helpers;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Nextify.Libs.Integracao.Classes
{
    public static class Emitente
    {
        public static List<EmitenteConfig> Config { get; set; }

        static Emitente()
        {
            Config = new List<EmitenteConfig>();
        }
    }

    public class EmitenteConfig
    {
        public EmitenteConfig(string cnpj, string ambiente, string certificado, string senha, string cscid, string csc, string uf, string instancia, string ativo)
        {
            Cnpj = cnpj;
            Ambiente = ambiente;
            Certificado = certificado;
            Senha = senha;
            CscId = cscid;
            Csc = csc;
            Uf = uf;
            Instancia = long.Parse(instancia);
            Ativo = bool.Parse(ativo);

            certificate = CertificateHelper.GetCertificateFromFile(Certificado, Senha);
        }

        public string Cnpj { get; set; }
        public string Ambiente { get; set; }
        public string Certificado { get; set; }
        public string Senha { get; set; }
        public string CscId { get; set; }
        public string Csc { get; set; }
        public string Uf { get; set; }
        public long Instancia { get; set; }
        public bool Ativo { get; set; }
        public X509Certificate2 certificate { get; set; }
}

    public static class IntegracaoSistemas
    {
        public static List<Sistema> Sistemas { get; set; }
        public static List<IntegracaoConfig> Config { get; set; }

        static IntegracaoSistemas()
        {
            Sistemas = new List<Sistema>();
            Config = new List<IntegracaoConfig>();
        }
    }

    public class Sistema
    {
        public Sistema(string instancia, string descricao, string tipointegracao, string ativo)
        {
            Instancia = long.Parse(instancia);
            Descricao = descricao;
            Tipointegracao = long.Parse(tipointegracao);
            Ativo = bool.Parse(ativo);
        }
        public long Instancia { get; set; }
        public string Descricao { get; set; }
        public long Tipointegracao { get; set; }
        public bool Ativo { get; set; }
    }

    public class IntegracaoConfig
    {
        public IntegracaoConfig(string instancia, IntegracaoApi api)
        {
            Instancia = long.Parse(instancia);
            Api = api;
        }
        public long Instancia { get; set; }
        public IntegracaoApi Api { get; set; }
    }

    public class IntegracaoApi
    {
        public IntegracaoApi(string url, string token, string apilistanfe, string apiretornonfe, string apixmlnfe, string apistatusservico)
        {
            Url = url;
            Token = token;
            ApiListaNFe = apilistanfe;
            ApiRetornoNFe = apiretornonfe;
            ApiXmlNFe = apixmlnfe;
            ApiStatusServico = apistatusservico;
        }
        public string Url { get; set; }
        public string Token { get; set; }
        public string ApiListaNFe { get; set; }
        public string ApiRetornoNFe { get; set; }
        public string ApiXmlNFe { get; set; }
        public string ApiStatusServico { get; set; }
    }
}
