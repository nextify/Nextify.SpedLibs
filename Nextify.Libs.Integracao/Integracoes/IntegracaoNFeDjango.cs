﻿using Nextify.Libs.Integracao.Enums;
using Nextify.Libs.Integracao.Classes;
using Nextify.Libs.Integracao.Interfaces;
using Nextify.Libs.NFe;
using Nextify.Libs.NFe.Classes;
using Nextify.NFe.Enums;
using System.Linq;
using System.Threading.Tasks;
using Nextify.NFe.Settings;
using System;

namespace Nextify.Libs.Integracao.Services
{
    public class IntegracaoNFeDjango : ServicosNFe, IServices
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public void ExecutarIntegracao(EmitenteConfig emitente)
        {
            try
            {
                var config = IntegracaoSistemas.Config.SingleOrDefault(n => n.Instancia == emitente.Instancia);
                RetornoStatusServicoNFe(config);
                var list = ObterListaNFe(config);
                if (list != null && list?.EventoNFe?.Count > 0)
                {
                    ProcessarListaNFe(emitente, config, list);
                    RetornoListaNFe(config, list);
                }
            } catch (Exception ex)
            {
                _logger.Error(ex.InnerException, "Ocorreu um erro durante a integração.");
            }
            
        }

        public NFeList ObterListaNFe(IntegracaoConfig config)
        {
            try
            {
                _logger.Trace("Obtendo listagem de Notas.");

                string api_url = config.Api.Url;
                string api_token = config.Api.Token;
                string api_param = config.Api.ApiListaNFe;

                Task<string> t = RestClient.get(api_url, api_param, api_token);

                NFeList list = NFeList.FromJson("{ EventoNFe: " + t.Result + "}");

                return list;
            } catch (Exception ex)
            {
                throw new Exception("Ocorreu um erro ao obter lista de notas.", ex.InnerException);
            } 
            
        }

        public void ProcessarListaNFe(EmitenteConfig emitente, IntegracaoConfig config, NFeList list)
        {
            try
            {
                _logger.Trace("Processando lista de notas.");

                string api_url = config.Api.Url;
                string api_token = config.Api.Token;
                string api_param = config.Api.ApiXmlNFe;

                foreach (var evt in list.EventoNFe)
                {
                    if (evt.Operacao == (int)NFeOperacao.NFeAutorizacao)
                    {
                        var param = $"{api_param}{evt.Nfe}/";
                        Task<string> t = RestClient.get(api_url, param, api_token);
                        string xml = t.Result;

                        EnviarLoteNFe(evt, emitente, xml);
                    }
                    else if (evt.Operacao == (int)NFeOperacao.NFeConsultaProtocolo) ConsultarSituacaoNFe(evt, emitente);
                    else if (evt.Operacao == (int)NFeOperacao.NFeRetAutorizacao) ConsultarProcessamentoLoteNFe(evt, emitente);
                    else if (evt.Operacao == (int)NFeOperacao.NFeRecepcaoEventoCCe) EventoCCeNFe(evt, emitente);
                    else if (evt.Operacao == (int)NFeOperacao.NFeRecepcaoEventoCancelamento) EventoCancelamentoNFe(evt, emitente);
                }
            } catch (Exception ex)
            {
                throw new Exception("Ocorreu um erro ao processar lista de notas.", ex.InnerException);
            }
            
        }

        public void RetornoListaNFe(IntegracaoConfig config, NFeList list)
        {
            try
            {
                _logger.Trace("Iniciando Retorno de Notas.");

                string api_url = config.Api.Url;
                string api_token = config.Api.Token;
                string api_param = config.Api.ApiRetornoNFe;

                foreach (var evt in list.EventoNFe)
                {
                    var param = $"{api_param}{evt.Id}/";
                    var content = Serialize.ToJson(evt);
                    Task<string> t = RestClient.put(api_url, param, api_token, content);
                }
            } catch (Exception ex)
            {
                throw new Exception("Ocorreu um erro ao retornar lista de notas.", ex.InnerException);
            }
            
        }

        public void RetornoStatusServicoNFe(IntegracaoConfig config)
        {
            try
            {
                _logger.Trace("Iniciando Retorno de Status de Serviço.");

                string api_url = config.Api.Url;
                string api_token = config.Api.Token;
                string api_param = config.Api.ApiStatusServico;

                foreach (var st in NFeSettings.NFeStatusList)
                {
                    if (st.TempoUltimoRetorno() > 15)
                    {
                        NFeWebServiceStatus status = new NFeWebServiceStatus();
                        status.Model = (long)st.Modelo;
                        status.Environment = (long)st.Ambiente;
                        status.StatusId = Int32.Parse(st.Status);
                        status.StatusDescription = st.Mensagem;
                        status.QueryDate = st.DataHoraConsulta.ToString("yyyy-MM-dd HH:mm:ss");
                        status.Webservice = st.UF;

                        var content = status.ToJson();
                        Task<string> t = RestClient.post(api_url, api_param, api_token, content);

                        st.DataHoraRetorno = DateTime.Now;
                    }
                }
            } catch (Exception ex)
            {
                throw new Exception("Ocorreu um erro ao enviar Status de Serviços de NFe.", ex.InnerException);
            }
        }
    }
}
