﻿using Nextify.Libs.Integracao.Classes;
using Nextify.Libs.Integracao.Interfaces;
using Nextify.NFe.Enums;
using Nextify.NFe.Settings;
using Nextify.Libs.NFe.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nextify.Libs.Integracao.Services
{
    public class IntegracaoNFe : ServicosNFe, IServices
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public void ExecutarIntegracao(EmitenteConfig emitente)
        {
            var config = IntegracaoSistemas.Config.SingleOrDefault(n => n.Instancia == emitente.Instancia);

            try
            {
                ConsultaStatusServicos(emitente, config);
            } catch (Exception ex)
            {
                _logger.Error(ex, "Ocorreu um erro durante a intergração de consulta de Status Serviço.");
            }
        }

        public void ConsultaStatusServicos(EmitenteConfig emitente, IntegracaoConfig config)
        {
            try
            {
                var ambientes = NFeSettings.NFeConfigSefazList.Where(n => n.Operacao == NFeOperacao.NFeStatusServico).ToList();
                if (NFeSettings.NFeStatusList.Count == 0)
                {
                    foreach (var ambiente in ambientes)
                    {
                        NFeStatusServico status = new NFeStatusServico();
                        status.Ambiente = ambiente.Ambiente;
                        status.Modelo = ambiente.Modelo;
                        status.UF = ambiente.UF;
                        status.DataHoraConsulta = DateTime.MinValue;
                        status.DataHoraRetorno = DateTime.MinValue;

                        NFeSettings.NFeStatusList.Add(status);
                    }
                }

                foreach (var status in NFeSettings.NFeStatusList)
                {
                    DateTime startTime = status.DataHoraConsulta;
                    DateTime endTime = DateTime.Now;
                    TimeSpan span = endTime.Subtract(startTime);

                    if (status.TempoUltimaConsulta() > 15)
                    {
                        ConsultarStatusServico(emitente, status);
                    }
                }
            } catch (Exception ex)
            {
                throw new Exception("Ocorreu um erro ao consultar status de serviços.", ex.InnerException);
            }
            
        }
    }
}
