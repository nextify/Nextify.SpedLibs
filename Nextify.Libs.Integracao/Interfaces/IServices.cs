﻿using Nextify.Libs.Integracao.Classes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nextify.Libs.Integracao.Interfaces
{
    public interface IServices
    {
        void ExecutarIntegracao(EmitenteConfig emitente);
    }
}
