﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Nextify.Libs.Integracao
{
    public static class RestClient
    {
        public static async Task<string> get(string base_url, string parameters, string token)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(base_url);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", token);

            // List data response.
            HttpResponseMessage response = client.GetAsync(parameters).Result; // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var str = await response.Content.ReadAsStringAsync();
                return str;
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return null;
            }
        }

        public static async Task<string> post(string base_url, string parameters, string token, string content)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(base_url);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", token);

            HttpResponseMessage response = client.PostAsJsonAsync(parameters, content).Result; // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                var str = await response.Content.ReadAsStringAsync();
                return str;
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return null;
            }
        }

        public static async Task<string> put(string base_url, string parameters, string token, string content)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(base_url);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", token);

            HttpResponseMessage response = client.PustAsJsonAsync(parameters, content).Result; // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                var str = await response.Content.ReadAsStringAsync();
                return str;
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return null;
            }
        }

        public static async Task<HttpResponseMessage> PostAsJsonAsync(this HttpClient client, string requestUrl, string content)
        {
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");
            return await client.PostAsync(requestUrl, stringContent);
        }

        public static async Task<HttpResponseMessage> PustAsJsonAsync(this HttpClient client, string requestUrl, string content)
        {
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");
            return await client.PutAsync(requestUrl, stringContent);
        }
    }
}