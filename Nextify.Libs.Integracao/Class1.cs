﻿using System;

namespace Nextify.Libs.Integracao
{
    public static class RestClient
    {
        public static async Task get()
        {
            string URL = "http://127.0.0.1:8000/";
            string urlParameters = "";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            //client.DefaultRequestHeaders.Add("Authorization", "10c95c2efb95f201826aa08ef3ae84bbe8e28db9");
            //client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Token 10c95c2efb95f201826aa08ef3ae84bbe8e28db9");



            // Add an Accept header for JSON format.
            //            client.DefaultRequestHeaders.Accept.Add(
            //                new MediaTypeWithQualityHeaderValue("text/xml"));

            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", "10c95c2efb95f201826aa08ef3ae84bbe8e28db9");

            // List data response.
            HttpResponseMessage response = client.GetAsync("nfce/nfce_api/22/").Result; // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                var str = await response.Content.ReadAsStringAsync();
                Console.Write(str);
                //                foreach (var d in dataObjects)
                //                {
                //                    Console.WriteLine($"{0}", d.Name);
                //                }
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }
        }
    }
}
