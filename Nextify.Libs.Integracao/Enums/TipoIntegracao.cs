﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nextify.Libs.Integracao.Enums
{
    public enum TipoIntegracao
    {
        NFeDjango = 0,
        NFeNode = 1,
        NFeServices = 2,
        Reinf = 3,
        eSocial = 4,
        CTe = 5,
        NFSe = 6
    }
}
