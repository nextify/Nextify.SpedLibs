﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nextify.Libs.Reinf.Enums
{
    public enum ReinfOperacao
    {
        XmlEventoAguardandoRecebimento,
        XmlEventoRecebido,
        XmlAguardandoAssinatura,
        XmlAssinado,
        XmlTransmitido
    }
}
