﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Nextify.Libs.Reinf.Helpers
{
    public class NextifyExtensions
    {
        public class StringWriterWithEncoding : StringWriter
        {
            Encoding encoding;
            public StringWriterWithEncoding(StringBuilder builder, Encoding encoding)
              : base(builder) { this.encoding = encoding; }
            public override Encoding Encoding { get { return encoding; } }
        }
    }
}
