﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Nextify.Libs.Reinf.Helpers
{
    public class SerializerHelper
    {
        public static string Serializar<T>(T protocolo)
        {
            var serializer = new XmlSerializer(typeof(T));
            string str;
            var sb = new StringBuilder();
            var sw = new NextifyExtensions.StringWriterWithEncoding(sb, Encoding.UTF8);
            XmlWriterSettings sett = new XmlWriterSettings();
            sett.OmitXmlDeclaration = true;
            var xtw = XmlTextWriter.Create(sw, sett);
            serializer.Serialize(xtw, protocolo);
            str = sb.ToString();
            xtw.Close();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(str);
            xml.DocumentElement.RemoveAttribute("xmlns:xsd");
            xml.DocumentElement.RemoveAttribute("xmlns:xsi");

            return xml.InnerXml;
        }

        public static T Deserializar<T>(string xml)
        {
            XmlSerializer serializerRetorno = new XmlSerializer(typeof(T));
            var serializer = new XmlSerializer(typeof(T));
            var sr = new StringReader(xml);
            var xtr = new XmlTextReader(sr);

            if (serializerRetorno.CanDeserialize(xtr))
            {
                var ret = (T)serializer.Deserialize(xtr);
                xtr.Close();
                return ret;
            }
            else
                throw new SerializationException("Não é possível deserializar o xml. Schema não confere");
        }

    }
}
