﻿//------------------------------------------------------------------------------
// <auto-generated>
//     O código foi gerado por uma ferramenta.
//     Versão de Tempo de Execução:4.0.30319.42000
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------
using Nextify.Libs.Reinf.Classes.Xml.Signature;
using System.Xml.Serialization;

// 
// This source code was auto-generated by xsd, Version=4.7.2558.0.
// 

namespace Nextify.Libs.Reinf.Classes.Xml.EvtInfoCPRB
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.reinf.esocial.gov.br/schemas/evtInfoCPRB/v1_04_00")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.reinf.esocial.gov.br/schemas/evtInfoCPRB/v1_04_00", IsNullable = false)]
    public partial class Reinf
    {

        private ReinfEvtCPRB evtCPRBField;

        private SignatureType signatureField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public ReinfEvtCPRB evtCPRB
        {
            get
            {
                return this.evtCPRBField;
            }
            set
            {
                this.evtCPRBField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.w3.org/2000/09/xmldsig#", Order = 1)]
        public SignatureType Signature
        {
            get
            {
                return this.signatureField;
            }
            set
            {
                this.signatureField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.reinf.esocial.gov.br/schemas/evtInfoCPRB/v1_04_00")]
    public partial class ReinfEvtCPRB
    {

        private ReinfEvtCPRBIdeEvento ideEventoField;

        private ReinfEvtCPRBIdeContri ideContriField;

        private ReinfEvtCPRBInfoCPRB infoCPRBField;

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public ReinfEvtCPRBIdeEvento ideEvento
        {
            get
            {
                return this.ideEventoField;
            }
            set
            {
                this.ideEventoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public ReinfEvtCPRBIdeContri ideContri
        {
            get
            {
                return this.ideContriField;
            }
            set
            {
                this.ideContriField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public ReinfEvtCPRBInfoCPRB infoCPRB
        {
            get
            {
                return this.infoCPRBField;
            }
            set
            {
                this.infoCPRBField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "ID")]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.reinf.esocial.gov.br/schemas/evtInfoCPRB/v1_04_00")]
    public partial class ReinfEvtCPRBIdeEvento
    {

        private byte indRetifField;

        private string nrReciboField;

        private string perApurField;

        private uint tpAmbField;

        private byte procEmiField;

        private string verProcField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public byte indRetif
        {
            get
            {
                return this.indRetifField;
            }
            set
            {
                this.indRetifField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string nrRecibo
        {
            get
            {
                return this.nrReciboField;
            }
            set
            {
                this.nrReciboField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "gYearMonth", Order = 2)]
        public string perApur
        {
            get
            {
                return this.perApurField;
            }
            set
            {
                this.perApurField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public uint tpAmb
        {
            get
            {
                return this.tpAmbField;
            }
            set
            {
                this.tpAmbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public byte procEmi
        {
            get
            {
                return this.procEmiField;
            }
            set
            {
                this.procEmiField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string verProc
        {
            get
            {
                return this.verProcField;
            }
            set
            {
                this.verProcField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.reinf.esocial.gov.br/schemas/evtInfoCPRB/v1_04_00")]
    public partial class ReinfEvtCPRBIdeContri
    {

        private byte tpInscField;

        private string nrInscField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public byte tpInsc
        {
            get
            {
                return this.tpInscField;
            }
            set
            {
                this.tpInscField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string nrInsc
        {
            get
            {
                return this.nrInscField;
            }
            set
            {
                this.nrInscField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.reinf.esocial.gov.br/schemas/evtInfoCPRB/v1_04_00")]
    public partial class ReinfEvtCPRBInfoCPRB
    {

        private ReinfEvtCPRBInfoCPRBIdeEstab ideEstabField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public ReinfEvtCPRBInfoCPRBIdeEstab ideEstab
        {
            get
            {
                return this.ideEstabField;
            }
            set
            {
                this.ideEstabField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.reinf.esocial.gov.br/schemas/evtInfoCPRB/v1_04_00")]
    public partial class ReinfEvtCPRBInfoCPRBIdeEstab
    {

        private uint tpInscEstabField;

        private string nrInscEstabField;

        private string vlrRecBrutaTotalField;

        private string vlrCPApurTotalField;

        private string vlrCPRBSuspTotalField;

        private ReinfEvtCPRBInfoCPRBIdeEstabTipoCod[] tipoCodField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public uint tpInscEstab
        {
            get
            {
                return this.tpInscEstabField;
            }
            set
            {
                this.tpInscEstabField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string nrInscEstab
        {
            get
            {
                return this.nrInscEstabField;
            }
            set
            {
                this.nrInscEstabField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string vlrRecBrutaTotal
        {
            get
            {
                return this.vlrRecBrutaTotalField;
            }
            set
            {
                this.vlrRecBrutaTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string vlrCPApurTotal
        {
            get
            {
                return this.vlrCPApurTotalField;
            }
            set
            {
                this.vlrCPApurTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string vlrCPRBSuspTotal
        {
            get
            {
                return this.vlrCPRBSuspTotalField;
            }
            set
            {
                this.vlrCPRBSuspTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("tipoCod", Order = 5)]
        public ReinfEvtCPRBInfoCPRBIdeEstabTipoCod[] tipoCod
        {
            get
            {
                return this.tipoCodField;
            }
            set
            {
                this.tipoCodField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.reinf.esocial.gov.br/schemas/evtInfoCPRB/v1_04_00")]
    public partial class ReinfEvtCPRBInfoCPRBIdeEstabTipoCod
    {

        private string codAtivEconField;

        private string vlrRecBrutaAtivField;

        private string vlrExcRecBrutaField;

        private string vlrAdicRecBrutaField;

        private string vlrBcCPRBField;

        private string vlrCPRBapurField;

        private ReinfEvtCPRBInfoCPRBIdeEstabTipoCodTipoAjuste[] tipoAjusteField;

        private ReinfEvtCPRBInfoCPRBIdeEstabTipoCodInfoProc[] infoProcField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string codAtivEcon
        {
            get
            {
                return this.codAtivEconField;
            }
            set
            {
                this.codAtivEconField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string vlrRecBrutaAtiv
        {
            get
            {
                return this.vlrRecBrutaAtivField;
            }
            set
            {
                this.vlrRecBrutaAtivField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string vlrExcRecBruta
        {
            get
            {
                return this.vlrExcRecBrutaField;
            }
            set
            {
                this.vlrExcRecBrutaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string vlrAdicRecBruta
        {
            get
            {
                return this.vlrAdicRecBrutaField;
            }
            set
            {
                this.vlrAdicRecBrutaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string vlrBcCPRB
        {
            get
            {
                return this.vlrBcCPRBField;
            }
            set
            {
                this.vlrBcCPRBField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string vlrCPRBapur
        {
            get
            {
                return this.vlrCPRBapurField;
            }
            set
            {
                this.vlrCPRBapurField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("tipoAjuste", Order = 6)]
        public ReinfEvtCPRBInfoCPRBIdeEstabTipoCodTipoAjuste[] tipoAjuste
        {
            get
            {
                return this.tipoAjusteField;
            }
            set
            {
                this.tipoAjusteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("infoProc", Order = 7)]
        public ReinfEvtCPRBInfoCPRBIdeEstabTipoCodInfoProc[] infoProc
        {
            get
            {
                return this.infoProcField;
            }
            set
            {
                this.infoProcField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.reinf.esocial.gov.br/schemas/evtInfoCPRB/v1_04_00")]
    public partial class ReinfEvtCPRBInfoCPRBIdeEstabTipoCodTipoAjuste
    {

        private byte tpAjusteField;

        private string codAjusteField;

        private string vlrAjusteField;

        private string descAjusteField;

        private string dtAjusteField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public byte tpAjuste
        {
            get
            {
                return this.tpAjusteField;
            }
            set
            {
                this.tpAjusteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "integer", Order = 1)]
        public string codAjuste
        {
            get
            {
                return this.codAjusteField;
            }
            set
            {
                this.codAjusteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string vlrAjuste
        {
            get
            {
                return this.vlrAjusteField;
            }
            set
            {
                this.vlrAjusteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string descAjuste
        {
            get
            {
                return this.descAjusteField;
            }
            set
            {
                this.descAjusteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "gYearMonth", Order = 4)]
        public string dtAjuste
        {
            get
            {
                return this.dtAjusteField;
            }
            set
            {
                this.dtAjusteField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.7.2558.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.reinf.esocial.gov.br/schemas/evtInfoCPRB/v1_04_00")]
    public partial class ReinfEvtCPRBInfoCPRBIdeEstabTipoCodInfoProc
    {

        private byte tpProcField;

        private string nrProcField;

        private string codSuspField;

        private string vlrCPRBSuspField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public byte tpProc
        {
            get
            {
                return this.tpProcField;
            }
            set
            {
                this.tpProcField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string nrProc
        {
            get
            {
                return this.nrProcField;
            }
            set
            {
                this.nrProcField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string codSusp
        {
            get
            {
                return this.codSuspField;
            }
            set
            {
                this.codSuspField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string vlrCPRBSusp
        {
            get
            {
                return this.vlrCPRBSuspField;
            }
            set
            {
                this.vlrCPRBSuspField = value;
            }
        }
    }
}