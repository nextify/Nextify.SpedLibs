﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nextify.Libs.Reinf.Classes.Api
{
    public class BaseEventoObject
    {
        public string _id { get; set; }
        public string idEvento { get; set; }
        public IdentificacaoEmpresaObject identificacaoEmpresa { get; set; }
        public IdentificacaoEventoObject identificacaoEvento { get; set; }
        public IdentificacaoContribuinteObject identificacaoContribuinte { get; set; }
        public IdentificacaoEstabelecimentoObject identificacaoEstabelecimento { get; set; }
    }

    public class IdentificacaoEmpresaObject
    {
        public string empresa { get; set; }
        public string estabelecimento { get; set; }
    }

    public class IdentificacaoEventoObject
    {
        public int? indRetificacao { get; set; }
        public string nrReciboRetificacao { get; set; }
        public DateTime? periodoApuracao { get; set; }
        public uint identificacaoAmbiente { get; set; }
        public uint processoEmissao { get; set; }
        public string versaoProcesso { get; set; }
    }

    public class IdentificacaoEstabelecimentoObject
    {
        public int tipoInscricaoEstabelecimento { get; set; }
        public string numeroInscricaoEstabelecimento { get; set; }
        public int? indObra { get; set; }
    }

    public class IdentificacaoContribuinteObject
    {
        public int tipoInscricao { get; set; }
        public string numeroInscricao { get; set; }
    }
}
