﻿using System;
using System.Collections.Generic;
using System.Text;
using Nextify.Libs.Reinf.Classes;
using EvtInfoContribuinte;
using Nextify.Libs.Reinf.Helpers;

namespace Nextify.Libs.Reinf.Classes.Api.Eventos
{
    public class EventoR1000Object : BaseEventoObject
    {

        public string ToXml()
        {
            EvtInfoContribuinte.Reinf reinf = new EvtInfoContribuinte.Reinf();

            reinf.evtInfoContri = new ReinfEvtInfoContri();
            reinf.evtInfoContri.id = idEvento;

            // Identificacao Contribuinte
            reinf.evtInfoContri.ideContri = new ReinfEvtInfoContriIdeContri();
            reinf.evtInfoContri.ideContri.tpInsc = (byte)identificacaoContribuinte.tipoInscricao;
            reinf.evtInfoContri.ideContri.nrInsc = identificacaoContribuinte.numeroInscricao;

            // Identificacao Evento
            reinf.evtInfoContri.ideEvento = new ReinfEvtInfoContriIdeEvento();
            reinf.evtInfoContri.ideEvento.procEmi = identificacaoEvento.processoEmissao;
            reinf.evtInfoContri.ideEvento.tpAmb = identificacaoEvento.identificacaoAmbiente;
            reinf.evtInfoContri.ideEvento.verProc = identificacaoEvento.versaoProcesso;


            // 
            reinf.evtInfoContri.infoContri = new ReinfEvtInfoContriInfoContri();


            return SerializerHelper.Serializar<EvtInfoContribuinte.Reinf>(reinf);
        }
    }
}
