﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nextify.Libs.Reinf.Classes.Api
{
    public class EmpresaObject
    {
        public string _id { get; set; }
        public string nome { get; set; }
        public bool principal { get; set; }
        public bool ativo { get; set; }
        public List<string> usuarios { get; set; }
        public List<EstabelecimentoObject> estabelecimentos { get; set; }
    }

    public class EstabelecimentoObject
    {
        public bool principal { get; set; }
        public string _id { get; set; }
        public string nome { get; set; }
        public string cnpj { get; set; }
    }
}
