﻿using System;
using System.IO;
using System.Net;

namespace Nextify.Libs.Reinf.Utils
{
    public enum HttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }

    public enum AuthenticationType
    {
        NoAuth,
        Basic,
        Bearer,
        NTLM
    }

    public class AuthData
    {
        public AuthenticationType AuthenticationType;

        public string AuthToken;
    }

    public class ResponseObject
    {
        public int respStatus { get; set; }
        public string[] errorMessages { get; set; }
        public string data { get; set; }
    }

    public class RestClient
    {
        public string endPoint { get; set; }
        public HttpVerb httpMethod { get; set; }
        public AuthData AuthData { get; set; }
        public string postJSON { get; set; }

        public ResponseObject MakeRequest()
        {
            string strResponseValue = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(endPoint);
            request.Method = httpMethod.ToString();

            if (AuthData.AuthenticationType == AuthenticationType.Bearer)
            {
                // String authHeaer = System.Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(userName + ":" + userPassword));
                request.Headers.Add("Authorization", $"Bearer {AuthData.AuthToken}");
            }

            if (request.Method == "POST" && postJSON != string.Empty)
            {
                request.ContentType = "application/json";
                using (StreamWriter swJSONPayload = new StreamWriter(request.GetRequestStream()))
                {
                    swJSONPayload.Write(postJSON);
                    swJSONPayload.Close();
                }
            }

            HttpWebResponse response = null;
            ResponseObject responseObject = new ResponseObject();

            try
            {
                response = (HttpWebResponse)request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            strResponseValue = reader.ReadToEnd();
                            responseObject.respStatus = (int)response.StatusCode;
                            responseObject.data = strResponseValue;
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                HttpWebResponse resp = (HttpWebResponse)ex.Response;
                responseObject.respStatus = (int)resp.StatusCode;
                responseObject.errorMessages = new string[] { ex.Message.ToString() };
            }
            catch (Exception ex)
            {
                responseObject.respStatus = -999;
                responseObject.errorMessages = new string[] { ex.Message.ToString() };
            }
            finally
            {
                if (response != null)
                {
                    ((IDisposable)response).Dispose();
                }
            }

            return responseObject;
        }
    }
}
