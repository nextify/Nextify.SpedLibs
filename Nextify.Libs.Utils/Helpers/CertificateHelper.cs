﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Nextify.Helpers
{
    public static class CertificateHelper
    {
        public static X509Certificate2 GetCertificateFromStore(string name, StoreLocation storeLocation = StoreLocation.LocalMachine)
        {
            X509Certificate2 oCert = null;
            X509Store oRepositorio = new X509Store("My", storeLocation);
            try
            {
                oRepositorio.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection oCertCollection = oRepositorio.Certificates;
                foreach (X509Certificate2 oCertTemp in oCertCollection)
                {
                    if (oCertTemp.Subject.Contains(name))
                    {
                        oCert = oCertTemp;
                        break;
                    }
                }
            }
            finally
            {
                oRepositorio.Close();
            }
            return oCert;

        }

        public static X509Certificate2 GetCertificateFromFile(string file, string password = null)
        {
            X509Certificate2 oCert = null;
            try
            {
                if (password != null)
                    oCert = new X509Certificate2(file, password);
                else
                    oCert = new X509Certificate2(file);
                return oCert;
            }
            catch (CryptographicException ex)
            {
                throw ex;
            }
        }
    }
}
