﻿using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;

namespace Nextify.Helpers
{
    public static class SignatureHelper
    {
        public static string _DigestMethod = SignedXml.XmlDsigSHA1Url;

        public static string _SignatureMethod = SignedXml.XmlDsigRSASHA1Url;

        public static bool _RemoveNamespace = false;

        public static bool _RSAPrivateKey = false;

        public static string AssinarXML(string xml, string bodyId, X509Certificate2 certificado)
        {
            var sr = new StringReader(xml);
            var xtr = new XmlTextReader(sr);
            // Passo 1: Obter os objetos principais: Documento XML e Certificado digital
            var oDocNFE = new XmlDocument();
            oDocNFE.Load(xtr);

            if (_RemoveNamespace)
            {
                oDocNFE.DocumentElement.RemoveAttribute("xmlns:xsd");
                oDocNFE.DocumentElement.RemoveAttribute("xmlns:xsi");
            }

            // Passo 2: Identificar e referenciar o "bloco" dentro do documento XML
            var oReference = new Reference();
            oReference.Uri = "#" + bodyId;

            // Passo 3: Aplicar os algoritmos de transformação
            oReference.DigestMethod = _DigestMethod;
            oReference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
            oReference.AddTransform(new XmlDsigC14NTransform());
            
            // Passo 4: Definir a chave de criptografia do algoritmo de assinatura assimétrica
            var oSignedXml = new SignedXml(oDocNFE);
            //oSignedXml.SigningKey = certificado.GetRSAPrivateKey();
            if (_RSAPrivateKey)
                oSignedXml.SigningKey = certificado.GetRSAPrivateKey();
            else
                oSignedXml.SigningKey = certificado.PrivateKey;

            oSignedXml.SignedInfo.SignatureMethod = _SignatureMethod;
            oSignedXml.AddReference(oReference);

            // Passo 5: Calcular a assinatura digital
            oSignedXml.ComputeSignature();
            // Passo 6: Adicionar o certificado ao documento NF-e assinado,
            var keyInfo = new KeyInfo();
            keyInfo.AddClause(new KeyInfoX509Data(certificado));
            oSignedXml.KeyInfo = keyInfo;

            // Passo 7: Obter o "bloco" que representa o XML da assinatura
            XmlElement oXmlElementoAssinatura = oSignedXml.GetXml();

            // Passo 8: Adicionar o Elemento de assinatura ao documento NF-e
            oDocNFE.DocumentElement.AppendChild(oDocNFE.ImportNode(oXmlElementoAssinatura, true));
            // Passo 9: Retornar o docmento NF-e assinado
            xml = oDocNFE.InnerXml;
            return xml;
        }

        //public static string AssinarXMLRSA256(string xml, string bodyId, X509Certificate2 certificado)
        //{
        //    var sr = new StringReader(xml);
        //    var xtr = new XmlTextReader(sr);
        //    // Passo 1: Obter os objetos principais: Documento XML e Certificado digital
        //    var oDocNFE = new XmlDocument();
        //    oDocNFE.Load(xtr);
        //    oDocNFE.DocumentElement.RemoveAttribute("xmlns:xsd");
        //    oDocNFE.DocumentElement.RemoveAttribute("xmlns:xsi");

        //    // Passo 2: Identificar e referenciar o "bloco" dentro do documento XML
        //    var oReference = new Reference();
        //    oReference.Uri = "#" + bodyId;

        //    // Passo 3: Aplicar os algoritmos de transformação
        //    oReference.DigestMethod = _DigestMethod;
        //    oReference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
        //    oReference.AddTransform(new XmlDsigC14NTransform());

        //    // Passo 4: Definir a chave de criptografia do algoritmo de assinatura assimétrica
        //    var oSignedXml = new SignedXml(oDocNFE);
        //    oSignedXml.SigningKey = certificado.GetRSAPrivateKey();

        //    //oSignedXml.SignedInfo.SignatureMethod = SignedXml.XmlDsigRSASHA1Url;
        //    oSignedXml.SignedInfo.SignatureMethod = SignedXml.XmlDsigRSASHA256Url;
        //    oSignedXml.AddReference(oReference);

        //    // Passo 5: Calcular a assinatura digital
        //    oSignedXml.ComputeSignature();
        //    // Passo 6: Adicionar o certificado ao documento NF-e assinado,
        //    var keyInfo = new KeyInfo();
        //    keyInfo.AddClause(new KeyInfoX509Data(certificado));
        //    oSignedXml.KeyInfo = keyInfo;

        //    // Passo 7: Obter o "bloco" que representa o XML da assinatura
        //    XmlElement oXmlElementoAssinatura = oSignedXml.GetXml();

        //    // Passo 8: Adicionar o Elemento de assinatura ao documento NF-e
        //    oDocNFE.DocumentElement.AppendChild(oDocNFE.ImportNode(oXmlElementoAssinatura, true));
        //    // Passo 9: Retornar o docmento NF-e assinado
        //    xml = oDocNFE.InnerXml;
        //    return xml;
        //}
    }
}
