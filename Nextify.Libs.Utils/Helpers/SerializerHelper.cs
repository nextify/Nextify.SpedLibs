﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Nextify.Util;

namespace Nextify.Helpers
{
    public static class SerializerHelper
    {
        public static string Serialize<T>(this T value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            string xml;
            XmlSerializer xmlserializer = new XmlSerializer(typeof(T));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "http://www.portalfiscal.inf.br/nfe");

            var sb = new StringBuilder();
            var sw = new StringWriterWithEncoding(sb, Encoding.UTF8);

            XmlWriterSettings sett = new XmlWriterSettings();
            sett.OmitXmlDeclaration = false;

            var xtw = XmlTextWriter.Create(sw, sett);

            xmlserializer.Serialize(xtw, value, ns);
            xml = sb.ToString();
            xtw.Close();
            xml = xml.Replace("<NFe>", "<NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\">");

            return xml;
        }

        public static T Deserialize<T>(string xml)
        {
            XmlSerializer serializerRetorno = new XmlSerializer(typeof(T));
            var serializer = new XmlSerializer(typeof(T));
            var sr = new StringReader(xml);
            var xtr = new XmlTextReader(sr);
            //var ret = new T();
            if (serializerRetorno.CanDeserialize(xtr))
            {
                var ret = (T)serializer.Deserialize(xtr);
                xtr.Close();
                return ret;
            }
            else
                throw new SerializationException("Não é possível deserializar o xml. Schema não confere");
        }

        public static string Serializar<T>(T protocolo)
        {
            var serializer = new XmlSerializer(typeof(T));
            string str;
            var sb = new StringBuilder();
            var sw = new StringWriterWithEncoding(sb, Encoding.UTF8);
            XmlWriterSettings sett = new XmlWriterSettings();
            sett.OmitXmlDeclaration = true;
            var xtw = XmlTextWriter.Create(sw, sett);
            serializer.Serialize(xtw, protocolo);
            str = sb.ToString();
            xtw.Close();

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(str);
            xml.DocumentElement.RemoveAttribute("xmlns:xsd");
            xml.DocumentElement.RemoveAttribute("xmlns:xsi");

            return xml.InnerXml;
        }

        //public static T Deserializar<T>(string xml)
        //{
        //    XmlSerializer serializerRetorno = new XmlSerializer(typeof(T));
        //    var serializer = new XmlSerializer(typeof(T));
        //    var sr = new StringReader(xml);
        //    var xtr = new XmlTextReader(sr);
        //    //var ret = new T();
        //    if (serializerRetorno.CanDeserialize(xtr))
        //    {
        //        var ret = (T)serializer.Deserialize(xtr);
        //        xtr.Close();
        //        return ret;
        //    }
        //    else
        //        throw new SerializationException("Não é possível deserializar o xml. Schema não confere");
        //}
    }


}
