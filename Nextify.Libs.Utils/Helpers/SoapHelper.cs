﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Nextify.Helpers
{
    public static class SoapHelper
    {
        static public string BuildSoap12(XmlNode xml, string ns)
        {
            StringBuilder env = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

            env.Append("<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ");
            env.Append("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" ");
            env.Append("xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">");

            env.Append("<soap12:Header />");

            env.Append("<soap12:Body>");
            env.Append($"<nfeDadosMsg xmlns=\"{ns}\">"); // mudar namespace para goiás
            env.Append(xml.LastChild.OuterXml.ToString());
            env.Append("</nfeDadosMsg>");

            env.Append("</soap12:Body>");
            env.Append("</soap12:Envelope>");

            return env.ToString();
        }
    }
}
