﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using Nextify.Helpers;

namespace Nextify.WSUtils
{
    public static class WS
    {
        public static string Request(XmlNode xml, string url, string method, string ns, X509Certificate2 cert)
        {
            try
            {
                string XMLRetorno = string.Empty;
                string xmlSoap = SoapHelper.BuildSoap12(xml, ns);

                Uri uri = new Uri(url);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);

                WebRequest webRequest = WebRequest.Create(uri);
                HttpWebRequest httpWR = (HttpWebRequest)webRequest;


                //X509Certificate2Collection certificates = new X509Certificate2Collection();
                //certificates.Import("D:\\Downloads\\ddmcert.pfx", "123456", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet);

                httpWR.ContentLength = Encoding.UTF8.GetBytes(xmlSoap).Length;

                httpWR.ClientCertificates.Add(cert);

                httpWR.ComposeContentType("application/soap+xml", Encoding.UTF8, method);

                httpWR.Method = "POST";

                Stream reqStream = httpWR.GetRequestStream();
                StreamWriter streamWriter = new StreamWriter(reqStream);
                streamWriter.Write(xmlSoap, 0, Encoding.UTF8.GetBytes(xmlSoap).Length);
                streamWriter.Close();

                WebResponse webResponse = httpWR.GetResponse();
                Stream respStream = webResponse.GetResponseStream();
                StreamReader streamReader = new StreamReader(respStream);

                XMLRetorno = streamReader.ReadToEnd();

                return XMLRetorno;
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    Console.WriteLine(reader.ReadToEnd());
                }
                throw;
            }
        }

        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
            //if (sslPolicyErrors == SslPolicyErrors.None)
            //    return true;
            //else
            //{
            //    return false;
            //}
        }
    }

    public static class ExtContentType
    {
        internal static void ComposeContentType(this HttpWebRequest http, string contentType, Encoding encoding, string action)
        {
            if (encoding == null && action == null)
                http.ContentType = contentType;

            StringBuilder stringBuilder = new StringBuilder(contentType);

            if (encoding != null)
            {
                stringBuilder.Append("; charset=");
                stringBuilder.Append(encoding.WebName);
            }

            if (action != null)
            {
                stringBuilder.Append("; action=\"");
                stringBuilder.Append(action);
                stringBuilder.Append("\"");
            }

            http.ContentType = stringBuilder.ToString();
        }
    }
}
