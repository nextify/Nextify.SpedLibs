﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Nextify.Util
{
    public static class XmlUtil
    {
        public static bool SaveXml(string xml, string file)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(file, true, Encoding.UTF8))
                {
                    writer.Write(xml);
                }
                return true;
            } catch (Exception ex)
            {
                return false;
            }
        }
    }
}
