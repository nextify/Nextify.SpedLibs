﻿using System;
using System.IO;
using System.Runtime.Loader;
using System.Threading;
using Microsoft.Extensions.Configuration;
using Nextify.NFe.Settings;
using Nextify.Libs.Integracao.Classes;
using Nextify.Libs.Integracao.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace Nextify.Libs.Service
{
    class Program
    {
        public static IConfigurationRoot Configuration { get; set; }

        static void Main(string[] args)
        {
            AssemblyLoadContext.Default.Unloading += SigTermEventHandler;
            Console.CancelKeyPress += CancelHandler;
            BuildConfiguration();

            var servicesProvider = BuildDi();
            var runner = servicesProvider.GetRequiredService<Runner>();

            runner.Run("Iniciando Serviço.");

            NLog.LogManager.Shutdown();
        }

        public class Runner
        {
            private readonly ILogger<Runner> _logger;

            public Runner(ILogger<Runner> logger)
            {
                _logger = logger;
            }

            public void Run(string name)
            {
                _logger.LogTrace("Iniciando Integrador de notas.");

                while (true)
                {
                    try
                    {
                        Servicos svc = new Servicos();
                        svc.Execute();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    finally
                    {
                    }
                    Thread.Sleep(2*60000);
                }
            }


        }

        private static void BuildConfiguration()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", false, true);
            Configuration = builder.Build();

            // Obter configuracoes de webservices
            var valuesSection = Configuration.GetSection("NFe:sefaz");
            foreach (IConfigurationSection section in valuesSection.GetChildren())
            {
                var modelo = section.GetSection("modelo").Value;
                var ambiente = section.GetSection("ambiente").Value;
                var uf = section.GetSection("uf").Value;
                var operacao = section.GetSection("operacao").Value;
                var url = section.GetSection("url").Value;
                var metodo = section.GetSection("metodo").Value;
                var urlqrcode = section.GetSection("urlqrcode").Value;
                var urlconsulta = section.GetSection("urlconsulta").Value;

                NFeSettings.AddConfigSefaz(modelo, ambiente, uf, operacao, url, metodo, urlqrcode, urlconsulta);
            }

            // Obter configuracoes gerais de NFe
            valuesSection = Configuration.GetSection("NFe:config");
            foreach (IConfigurationSection section in valuesSection.GetChildren())
            {
                var ambiente = section.GetSection("ambiente");
                var dir_base_xml = section.GetSection("dir_base_xml");
                var ativo = section.GetSection("ativo");

                NFeSettings.AddConfig(ambiente.Value, dir_base_xml.Value, ativo.Value);
            }

            // Obter configuracoes de Emissores
            valuesSection = Configuration.GetSection("Emitente:config");
            foreach (IConfigurationSection section in valuesSection.GetChildren())
            {
                var cnpj = section.GetSection("cnpj").Value;
                var ambiente = section.GetSection("ambiente").Value;
                var certificado = section.GetSection("certificado").Value;
                var senha = section.GetSection("senha").Value;
                var cscid = section.GetSection("cscid").Value;
                var csc = section.GetSection("csc").Value;
                var uf = section.GetSection("uf").Value;
                var instancia = section.GetSection("instancia").Value;
                var ativo = section.GetSection("ativo").Value;

                var emitente = new EmitenteConfig(cnpj, ambiente, certificado, senha, cscid, csc, uf, instancia, ativo);
                Emitente.Config.Add(emitente);
            }

            valuesSection = Configuration.GetSection("Integracao:sistemas");
            // Obter configuracoes de Api de Integracao
            foreach (IConfigurationSection section in valuesSection.GetChildren())
            {
                var instancia = section.GetSection("instancia").Value;
                var descricao = section.GetSection("descricao").Value;
                var tipointegracao = section.GetSection("tipointegracao").Value;
                var ativo = section.GetSection("ativo").Value;

                var sistema = new Sistema(instancia, descricao, tipointegracao, ativo);
                IntegracaoSistemas.Sistemas.Add(sistema);
            }

            valuesSection = Configuration.GetSection("Integracao:config");
            // Obter configuracoes de Api de Integracao
            foreach (IConfigurationSection section in valuesSection.GetChildren())
            {
                var instancia = section.GetSection("instancia").Value;

                var url = section.GetSection("api:url").Value;
                var token = section.GetSection("api:token").Value;
                var listanfe = section.GetSection("api:listanfe").Value;
                var retornonfe = section.GetSection("api:retornonfe").Value;
                var xmlnfe = section.GetSection("api:xmlnfe").Value;
                var statusservico = section.GetSection("api:statusservico").Value;

                var api = new IntegracaoApi(url, token, listanfe, retornonfe, xmlnfe, statusservico);
                var config = new IntegracaoConfig(instancia, api);
                IntegracaoSistemas.Config.Add(config);
            }
        }

        private static IServiceProvider BuildDi()
        {
            var services = new ServiceCollection();

            //Runner is the custom class
            services.AddTransient<Runner>();

            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
            services.AddLogging((builder) => builder.SetMinimumLevel(LogLevel.Trace));

            var serviceProvider = services.BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            //configure NLog
            loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });
            NLog.LogManager.LoadConfiguration("nlog.config");

            return serviceProvider;
        }

        private static void SigTermEventHandler(AssemblyLoadContext obj)
        {
            System.Console.WriteLine("Unloading...");
        }

        private static void CancelHandler(object sender, ConsoleCancelEventArgs e)
        {
            System.Console.WriteLine("Exiting...");
            Environment.Exit(0);
        }
    }
}
